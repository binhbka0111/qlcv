package com.binhdi0111.bka.customview_mvp.Presenter;

import android.graphics.Color;
import android.widget.Spinner;

import com.binhdi0111.bka.customview_mvp.Adapter.CustomAdaperSpinner;
import com.binhdi0111.bka.customview_mvp.Interface.InterfaceStatistical;
import com.binhdi0111.bka.customview_mvp.Object.spinner;
import com.binhdi0111.bka.customview_mvp.Activity.statistical;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.util.ArrayList;

public class statisticalPresenter {
    statistical context;
    InterfaceStatistical interfaceStatistical;
    ArrayList<spinner> arrayList;
    CustomAdaperSpinner adapter;

    public statisticalPresenter(InterfaceStatistical interfaceStatistical) {
        this.interfaceStatistical = interfaceStatistical;
    }
    public void onClickChangeLayout(int index){
        switch (index){
            case 0:
                interfaceStatistical.SelectTab0();
                break;
            case 1:
                interfaceStatistical.SelectTab1();
                break;
        }
    }
    public void SetAdapterSpinner(int index,Spinner spinner){
        switch (index){
            case 1:
                arrayList = new ArrayList<>();
                arrayList.add(new spinner("Tất cả"));
                arrayList.add(new spinner("hihi"));
                arrayList.add(new spinner("haha"));
                arrayList.add(new spinner("hoho"));
                adapter = new CustomAdaperSpinner(arrayList);
                spinner.setAdapter(adapter);
                break;
            case 2:
                arrayList = new ArrayList<>();
                arrayList.add(new spinner("Thời gian tạo"));
                arrayList.add(new spinner("Hạn hoàn thành"));
                arrayList.add(new spinner("Ngày bắt đầu"));
                adapter= new CustomAdaperSpinner(arrayList);
                spinner.setAdapter(adapter);
                break;
            case 3:
                arrayList = new ArrayList<>();
                arrayList.add(new spinner("Tuần"));
                arrayList.add(new spinner("Ngày"));
                arrayList.add(new spinner("Tháng"));
                arrayList.add(new spinner("Năm"));
                adapter = new CustomAdaperSpinner(arrayList);
                spinner.setAdapter(adapter);
                break;
            case 4:
                arrayList = new ArrayList<>();
                arrayList.add(new spinner("Từ 19-26/09/2022"));
                arrayList.add(new spinner("Từ 11-18/09/2022"));
                arrayList.add(new spinner("Từ 03-10/09/2022"));
                arrayList.add(new spinner("Từ 27-02/10/2022"));
                adapter = new CustomAdaperSpinner(arrayList);
                spinner.setAdapter(adapter);
        }
    }
    public void PieChart(PieChart pieChart) {
        ArrayList<PieEntry> yEntrys = new ArrayList<>();
        ArrayList<String> xEntrys = new ArrayList<>();
        float[] yData = { 10, 20, 30,30 ,10};
        String[] xData = { "", "", "","","" };

        for (int i = 0; i < yData.length;i++){
            yEntrys.add(new PieEntry(yData[i],i));
        }
        for (int i = 0; i < xData.length;i++){
            xEntrys.add(xData[i]);
        }

        PieDataSet pieDataSet=new PieDataSet(yEntrys,"");
        pieDataSet.setSliceSpace(2);
        pieDataSet.setValueTextSize(12);

        ArrayList<Integer> colors=new ArrayList<>();
        colors.add(Color.rgb(238,235,235));
        colors.add(Color.rgb(76,166,65));
        colors.add(Color.rgb(236,156,82));
        colors.add(Color.rgb(58,119,252));
        colors.add(Color.rgb(226,93,91));

        pieDataSet.setColors(colors);
        Legend legend=pieChart.getLegend();
        legend.setForm(Legend.LegendForm.CIRCLE);
        legend.setPosition(Legend.LegendPosition.LEFT_OF_CHART);

        PieData pieData=new PieData(pieDataSet);
        pieData.setDrawValues(false);
        pieChart.setData(pieData);
        pieChart.invalidate();
        pieChart.setRotationEnabled(true);
        pieChart.setDescription(new Description());
        pieChart.setHoleRadius(35f);
        pieChart.setTransparentCircleAlpha(0);
        pieChart.setCenterText("Tổng\n"+10);
        pieChart.setCenterTextSize(10);
        pieChart.setDrawEntryLabels(true);
        pieChart.getLegend().setEnabled(false);
        pieChart.getDescription().setEnabled(false);
        pieChart.setDrawSliceText(false);
        float x = 70;
        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleRadius(x);
        pieChart.setOnChartValueSelectedListener((OnChartValueSelectedListener) context);
    }
}
