package com.binhdi0111.bka.customview_mvp.Object;

public class Staff {
    private String name;
    private String role;
    private int avatar;

    public Staff(String name, String role, int avatar) {
        this.name = name;
        this.role = role;
        this.avatar = avatar;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getAvatar() {
        return avatar;
    }

    public void setAvatar(int avatar) {
        this.avatar = avatar;
    }


}
