package com.binhdi0111.bka.customview_mvp.BottomSheetDialogFragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.binhdi0111.bka.customview_mvp.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class BottomSheetDialogDetailJob extends BottomSheetDialogFragment {
    static BottomSheetDialogDetailJob bottomSheetDialogDetailJob;
    TextView txtTitle,txtLabel,txtNote;
    LinearLayout lnCancel,lnConfirm;
    EditText edtContent;


    //Get instance
    //ListenerA
    // listener = ListenerA


   static DialogDetailJob listener;

    public BottomSheetDialogDetailJob(DialogDetailJob listener) {
        this.listener = listener;
    }

    @SuppressLint("MissingInflatedId")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottom_sheet_dialog_detail_job,container,false);
        txtLabel = (TextView) view.findViewById(R.id.detailJobLabel);
        txtTitle = (TextView) view.findViewById(R.id.textViewTitlejob);
        txtNote = (TextView) view.findViewById(R.id.textViewNote);
        edtContent = (EditText) view.findViewById(R.id.editTextContentDetailJob);
        lnCancel = (LinearLayout) view.findViewById(R.id.linearLayoutCancel);
        lnConfirm = (LinearLayout) view.findViewById(R.id.linearLayoutConfirm);
        lnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String content = edtContent.getText().toString().trim();
                listener.clickListener(content);
                bottomSheetDialogDetailJob.dismiss();
            }
        });
        lnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogDetailJob.dismiss();
            }
        });
        Bundle bundle = this.getArguments();
        Log.d("hihihihihihi", "onCreateView: "+bundle);
        if (bundle != null) {
            String label = bundle.getString("label");
            String title = bundle.getString("title");
            String require = bundle.getString("require");
            txtLabel.setText(label);
            txtTitle.setText(title);
            txtNote.setText(require);
        }
        return view;
    }
    public static BottomSheetDialogDetailJob newInstance(DialogDetailJob listener) {
        bottomSheetDialogDetailJob= new BottomSheetDialogDetailJob(listener);
        return bottomSheetDialogDetailJob;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(bottomSheetDialogDetailJob.STYLE_NORMAL, R.style.DialogStyle);
    }
    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        View contentView = View.inflate(getContext(), R.layout.bottom_sheet_dialog_detail_job, null);
        dialog.setContentView(contentView);
        ((View) contentView.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
    }
    public interface DialogDetailJob {
        public void clickListener(String content);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (DialogDetailJob) getActivity();
        }
        catch (ClassCastException e) {
            Log.e("000000000000", "onAttach: ClassCastException: "
                    + e.getMessage());
        }
    }
}
