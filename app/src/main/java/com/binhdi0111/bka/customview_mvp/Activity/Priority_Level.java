package com.binhdi0111.bka.customview_mvp.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.binhdi0111.bka.customview_mvp.Adapter.AdapterPriorityLevel;
import com.binhdi0111.bka.customview_mvp.BottomSheetDialogFragment.BottomSheetConfirmJob;
import com.binhdi0111.bka.customview_mvp.Interface.IntefacePriorityLevel;
import com.binhdi0111.bka.customview_mvp.Object.Status;
import com.binhdi0111.bka.customview_mvp.Presenter.PriorityLevelPresenter;
import com.binhdi0111.bka.customview_mvp.R;

import java.util.ArrayList;

public class Priority_Level extends AppCompatActivity implements IntefacePriorityLevel {
    Spinner spinnerPriorityLevel;
    Button btnUpdate,btnCancel;
    BottomSheetConfirmJob bottomSheetConfirmJob;
    ImageView imgPriorityLevelBack;
    PriorityLevelPresenter priorityLevelPresenter;
    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_priority_level);
        spinnerPriorityLevel = (Spinner) findViewById(R.id.SpinnerPriorityLevel);
        priorityLevelPresenter = new PriorityLevelPresenter(this);
        priorityLevelPresenter.SetAdapterSpinner(spinnerPriorityLevel);
        btnUpdate =(Button) findViewById(R.id.buttonSavePriorityLevel);
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetConfirmJob = BottomSheetConfirmJob.newInstance(new BottomSheetConfirmJob.DialogUpdate() {
                    @Override
                    public void clicklistenerUpdateJobGroup(String content) {

                    }

                    @Override
                    public void clickListenerPriorityLevel(String content, int color) {
                        Intent sendcontent = new Intent("send_content_prioritylevel_data");
                        Bundle bundle = new Bundle();
                        bundle.putString("sendcontent",content);
                        bundle.putInt("sendcolor",color);
                        sendcontent.putExtra("data",bundle);
                        sendBroadcast(sendcontent);
                        onBackPressed();
                    }
                });
                Bundle bundle = new Bundle();
                bundle.putString("contentPriorityLevel",priorityLevelPresenter.ResultContent());
                bundle.putInt("colorPriorityLevel",priorityLevelPresenter.ResultColor());
                bundle.putString("title","Bạn có muốn cập nhật mức độ ưu tiên cho công việc hiện tai?");
                bottomSheetConfirmJob.setArguments(bundle);
                bottomSheetConfirmJob.show(getSupportFragmentManager(), bottomSheetConfirmJob.getTag());
            }
        });
        btnCancel =(Button) findViewById(R.id.buttonCancelPriorityLevel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        imgPriorityLevelBack = (ImageView) findViewById(R.id.imgPriorityBack);
        imgPriorityLevelBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}
