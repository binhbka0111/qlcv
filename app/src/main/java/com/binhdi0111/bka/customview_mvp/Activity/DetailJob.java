package com.binhdi0111.bka.customview_mvp.Activity;

import android.animation.LayoutTransition;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

import com.binhdi0111.bka.customview_mvp.BottomSheetDialogFragment.BottomSheetDialogDetailJob;
import com.binhdi0111.bka.customview_mvp.BottomSheetDialogFragment.BottomSheetDialogDocument;
import com.binhdi0111.bka.customview_mvp.BottomSheetDialogFragment.BottomSheetDialogFragmentDate;
import com.binhdi0111.bka.customview_mvp.BroadCastReceiver;
import com.binhdi0111.bka.customview_mvp.R;

public class DetailJob extends AppCompatActivity {
    TextView txtNameJob, txtJobDescription, txtTimeStart, txtTimeEnd,txtJobGroup,txtPriorityLevel;
    BottomSheetDialogDetailJob bottomSheetDialogDetailJob;
    BottomSheetDialogFragmentDate bottomSheetDialogFragmentDate;
    LinearLayout lnNameJob,lnJobDescription,lnTimeStart,lnTimeEnd;
    ImageView imgGroupJob,imgPriorityLevel,imgStatusPriorityLevel,imgBack,imgAddDocument;
    ImageView imgDrop,imgEdit;
    BottomSheetDialogDocument bottomSheetDialogDocument;
    TextView edtSymbolcode,edtNameDocument,edtDateDocumnet,edtNoteDocumnet;
    LinearLayout lnNameDocument,lnExpandDocument;
    BroadCastReceiver broadCastReceiverPriorityLevel = new BroadCastReceiver(){
        @SuppressLint("SetTextI18n")
        @Override
        public void onReceive(Context context, Intent intent) {
            super.onReceive(context, intent);
            if (intent != null){
                Bundle bundle = intent.getBundleExtra("data");
                String priorityLevel = bundle.getString("sendcontent");
                int color = bundle.getInt("sendcolor");
                txtPriorityLevel = (TextView)findViewById(R.id.textViewProrityLevel);
                imgStatusPriorityLevel = (ImageView) findViewById(R.id.imageViewStatusPriorityLevel);
                if(txtPriorityLevel.equals("Chưa chọn mức độ ưu tiên")){

                    imgGroupJob.setImageResource(R.drawable.ic_baseline_add_circle_24);
                    imgStatusPriorityLevel.setImageResource(R.drawable.ic_baseline_circle_24_1);
                }else {
                    txtPriorityLevel.setText(priorityLevel);
                    imgGroupJob.setImageResource(R.drawable.ic_baseline_edit_24);
                    imgStatusPriorityLevel.setImageResource(color);
                }
            }
        }
    };
    BroadCastReceiver broadCastReceiver = new BroadCastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent) {
            super.onReceive(context, intent);
            if (intent != null){
                String jobgroup = intent.getStringExtra("sendcontent");
                txtJobGroup = (TextView)findViewById(R.id.textViewJobGroup);
                if(jobgroup.equals("Chưa chọn nhóm công việc")){
                    txtJobGroup.setText("Chưa chọn nhóm công việc");
                    imgGroupJob.setImageResource(R.drawable.ic_baseline_add_circle_24);
                }else {
                    txtJobGroup.setText(jobgroup);
                    imgGroupJob.setImageResource(R.drawable.ic_baseline_edit_24);
                }
            }
        }
    };


    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_job);
        txtNameJob = (TextView) findViewById(R.id.edtcontenttencv);
        txtJobDescription = (TextView) findViewById(R.id.edtcontentmota);
        txtTimeStart = (TextView) findViewById(R.id.edtTimeStart);
        txtTimeEnd = (TextView) findViewById(R.id.edtTimeEnd);
        imgDrop = (ImageView)findViewById(R.id.imageViewDropDocument);
        lnNameJob = (LinearLayout) findViewById(R.id.linearLayoutNameJob);
        lnJobDescription = (LinearLayout) findViewById(R.id.linearLayoutJobDescription);
        lnTimeStart = (LinearLayout) findViewById(R.id.linearLayoutTimeStart);
        lnTimeEnd= (LinearLayout) findViewById(R.id.linearLayoutTimeEnd);
        imgGroupJob = (ImageView)findViewById(R.id.imageViewGroupJob);
        edtSymbolcode = (TextView) findViewById(R.id.editTextSymbolCodeDetailJob);
        edtNameDocument = (TextView) findViewById(R.id.editTextNameDocumentDetailJob);
        edtDateDocumnet = (TextView) findViewById(R.id.editTextDateDocumentDetailJob);
        edtNoteDocumnet = (TextView) findViewById(R.id.editTextnoteDocumentDetailJob);
        lnNameDocument = (LinearLayout)findViewById(R.id.linearLayoutNameDocument);
        lnExpandDocument = (LinearLayout)findViewById(R.id.linearLayoutExpand);
        imgEdit = (ImageView)findViewById(R.id.imageViewEdit);
        imgBack =(ImageView)findViewById(R.id.imgDetailJobBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        imgAddDocument = (ImageView)findViewById(R.id.imageViewAddDocument);
        imgAddDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogDocument = BottomSheetDialogDocument.newInstance(new BottomSheetDialogDocument.DialogDocument() {
                    @Override
                    public void clickListener(String symbolCode, String Name, String Date, String Note) {
                        edtSymbolcode.setText(symbolCode);
                        edtNameDocument.setText(Name);
                        edtDateDocumnet.setText(Date);
                        edtNoteDocumnet.setText(Note);
                    }
                });
                imgEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Bundle bundle = new Bundle();
                        bundle.putString("symbolcode",edtSymbolcode.getText().toString());
                        bundle.putString("name",edtNameDocument.getText().toString());
                        bundle.putString("date",edtDateDocumnet.getText().toString());
                        bundle.putString("note",edtNoteDocumnet.getText().toString());
                        bottomSheetDialogDocument.setArguments(bundle);
                        bottomSheetDialogDocument.show(getSupportFragmentManager(),bottomSheetDialogDocument.getTag());
                    }
                });

                bottomSheetDialogDocument.show(getSupportFragmentManager(),bottomSheetDialogDocument.getTag());
            }
        });

        IntentFilter filter = new IntentFilter("send_content_jobgroup_data");
        IntentFilter filterPriorityLevel = new IntentFilter("send_content_prioritylevel_data");
        registerReceiver(broadCastReceiver,filter);
        registerReceiver(broadCastReceiverPriorityLevel,filterPriorityLevel);
        imgGroupJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DetailJob.this,UpdateJobGroup.class);
                startActivity(intent);
            }
        });

        imgPriorityLevel =(ImageView)findViewById(R.id.imageViewPriorityLevel);
        imgPriorityLevel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent piorityLevel = new Intent(DetailJob.this,Priority_Level.class);
                startActivity(piorityLevel);
            }
        });


        lnNameJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogDetailJob = BottomSheetDialogDetailJob.newInstance(new BottomSheetDialogDetailJob.DialogDetailJob() {
                    @Override
                    public void clickListener(String content) {
                        txtNameJob.setText(content);
                    }
                });
                Bundle bundle = new Bundle();
                bundle.putString("label","Cập nhật tên công việc");
                bundle.putString("title","Tên công việc");
                bundle.putString("require", "(*)");
                bottomSheetDialogDetailJob.setArguments(bundle);
                bottomSheetDialogDetailJob.show(getSupportFragmentManager(), bottomSheetDialogDetailJob.getTag());
            }
        });
        lnJobDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogDetailJob = BottomSheetDialogDetailJob.newInstance(new BottomSheetDialogDetailJob.DialogDetailJob() {
                    @Override
                    public void clickListener(String content) {
                        txtJobDescription.setText(content);
                    }
                });
                Bundle bundle = new Bundle();
                bundle.putString("label","Cập nhật Mô tả công việc");
                bundle.putString("title","Mô tả công việc");
                bundle.putString("require","");
                bottomSheetDialogDetailJob.setArguments(bundle);
                bottomSheetDialogDetailJob.show(getSupportFragmentManager(), bottomSheetDialogDetailJob.getTag());
            }
        });
        lnTimeStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogFragmentDate =BottomSheetDialogFragmentDate.newInstance(new BottomSheetDialogFragmentDate.DialogDateJob() {
                    @Override
                    public void clickListenerDate(String date, String time) {
                        txtTimeStart.setText(date + " " + time);
                    }
                });
                Bundle bundle = new Bundle();
                bundle.putString("label","Cập nhật thời gian bắt đầu");
                bundle.putString("title","Thời gian bắt đầu");
                bottomSheetDialogFragmentDate.setArguments(bundle);
                bottomSheetDialogFragmentDate.show(getSupportFragmentManager(),bottomSheetDialogFragmentDate.getTag());
            }
        });
        lnTimeEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogFragmentDate =BottomSheetDialogFragmentDate.newInstance(new BottomSheetDialogFragmentDate.DialogDateJob() {
                    @Override
                    public void clickListenerDate(String date, String time) {
                        txtTimeEnd.setText(date + " " + time);
                    }
                });
                Bundle bundle = new Bundle();
                bundle.putString("label","Cập nhật thời hạn hoàn thành");
                bundle.putString("title","Hạn hoàn thành");
                bottomSheetDialogFragmentDate.setArguments(bundle);
                bottomSheetDialogFragmentDate.show(getSupportFragmentManager(),bottomSheetDialogFragmentDate.getTag());
            }
        });
        lnNameDocument.getLayoutTransition().enableTransitionType(LayoutTransition.CHANGING);


    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadCastReceiver);
        unregisterReceiver(broadCastReceiverPriorityLevel);
    }
    public void expanDocument(View view){
        int v = (lnExpandDocument.getVisibility()==View.GONE)?View.VISIBLE: View.GONE;
        TransitionManager.beginDelayedTransition(lnNameDocument,new AutoTransition());
        lnExpandDocument.setVisibility(v);
        if (lnExpandDocument.getVisibility()== View.VISIBLE){
            imgDrop.setImageResource(R.drawable.ic_baseline_keyboard_arrow_up_24);
        }else {
            imgDrop.setImageResource(R.drawable.ic_baseline_keyboard_arrow_down_24);
        }
    }



}
