package com.binhdi0111.bka.customview_mvp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.binhdi0111.bka.customview_mvp.Activity.DetailJob;
import com.binhdi0111.bka.customview_mvp.Interface.ItemClickListener;
import com.binhdi0111.bka.customview_mvp.Object.JobStatus;
import com.binhdi0111.bka.customview_mvp.R;

import java.util.ArrayList;

public class AdapterJob extends RecyclerView.Adapter<AdapterJob.ViewHolder>{
    Context context;
    ArrayList<JobStatus> arrayList;
    ItemClickListener itemClickListener;

    public AdapterJob(Context context, ArrayList<JobStatus> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_job,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        JobStatus jobStatus = arrayList.get(position);
        holder.txtName.setText(jobStatus.getName());
        holder.txtTitle.setText(jobStatus.getTitle());
        holder.txtStatus.setText(jobStatus.getStatus());
        holder.txtTime.setText(jobStatus.getTime());
        holder.txtStatus.setTextColor(jobStatus.getColor());
        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int adapterPosition) {
                    Intent intent = new Intent(context, DetailJob.class);
                    context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txtName,txtTitle,txtStatus,txtTime;
        private ItemClickListener itemClickListener;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.textViewTitle);
            txtStatus = (TextView) itemView.findViewById(R.id.textViewStatus);
            txtName = (TextView) itemView.findViewById(R.id.textViewName);
            txtTime = (TextView) itemView.findViewById(R.id.textViewtime1);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClick(view,getAdapterPosition());
        }

        public void setItemClickListener(ItemClickListener itemClickListener)
        {
            this.itemClickListener = itemClickListener;
        }

    }
}
