package com.binhdi0111.bka.customview_mvp.Presenter;


import android.content.Context;
import android.graphics.Color;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.binhdi0111.bka.customview_mvp.Adapter.AdapterRoom;
import com.binhdi0111.bka.customview_mvp.Adapter.CustomAdaperSpinner;
import com.binhdi0111.bka.customview_mvp.Fragment.Fragment_Dashboard;
import com.binhdi0111.bka.customview_mvp.Interface.InterfaceDashboard;
import com.binhdi0111.bka.customview_mvp.Object.Room;
import com.binhdi0111.bka.customview_mvp.Object.spinner;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class DashboardPresenter {
    Fragment_Dashboard context;
    public InterfaceDashboard interfaceDashboard;
    SimpleDateFormat simpleDateFormat;
    Calendar calendar;
    ArrayList<Room> arrayListRoom;
    AdapterRoom adapterRoom;
    ArrayList<spinner> arrayList;
    CustomAdaperSpinner adapter;

    public DashboardPresenter(InterfaceDashboard interfaceDashboard) {
        this.interfaceDashboard = interfaceDashboard;
    }

    public void onClickChangeLayout(int index){
        switch (index){
            case 0:
                interfaceDashboard.SelectTab0();
                break;
            case 1:
                interfaceDashboard.SelectTab1();
                break;
        }
    }
    public void DatePicker(TextView textView){
        calendar = Calendar.getInstance();
        simpleDateFormat = new SimpleDateFormat("hh:mm dd/MM/yyyy");
        textView.setText(simpleDateFormat.format(calendar.getTime()));
    }
    public void PieChart(PieChart pieChart) {
        ArrayList<PieEntry> yEntrys = new ArrayList<>();
        ArrayList<String> xEntrys = new ArrayList<>();
        float[] yData = { 10, 20, 30,30 ,10};
        String[] xData = { "", "", "","","" };

        for (int i = 0; i < yData.length;i++){
            yEntrys.add(new PieEntry(yData[i],i));
        }
        for (int i = 0; i < xData.length;i++){
            xEntrys.add(xData[i]);
        }

        PieDataSet pieDataSet=new PieDataSet(yEntrys,"");
        pieDataSet.setSliceSpace(2);
        pieDataSet.setValueTextSize(12);

        ArrayList<Integer> colors=new ArrayList<>();
        colors.add(Color.rgb(238,235,235));
        colors.add(Color.rgb(76,166,65));
        colors.add(Color.rgb(236,156,82));
        colors.add(Color.rgb(58,119,252));
        colors.add(Color.rgb(226,93,91));

        pieDataSet.setColors(colors);
        Legend legend=pieChart.getLegend();
        legend.setForm(Legend.LegendForm.CIRCLE);
        legend.setPosition(Legend.LegendPosition.LEFT_OF_CHART);

        PieData pieData=new PieData(pieDataSet);
        pieData.setDrawValues(false);
        pieChart.setData(pieData);
        pieChart.invalidate();
        pieChart.setRotationEnabled(true);
        pieChart.setDescription(new Description());
        pieChart.setHoleRadius(35f);
        pieChart.setTransparentCircleAlpha(0);
        pieChart.setCenterText("Tổng\n"+10);
        pieChart.setCenterTextSize(10);
        pieChart.setDrawEntryLabels(true);
        pieChart.getLegend().setEnabled(false);
        pieChart.getDescription().setEnabled(false);
        pieChart.setDrawSliceText(false);
        float x = 70;
        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleRadius(x);
        pieChart.setOnChartValueSelectedListener((OnChartValueSelectedListener) context);
    }
    public void TransferActivity(int index){
        switch (index){
            case 1: interfaceDashboard.goStatistical();break;
            case 2: interfaceDashboard.goPersonnel();break;
            case 3: interfaceDashboard.goCreateJob();break;
            case 4: interfaceDashboard.goMyJob();break;
        }
    }

    public void SetAdapterSpinner(int index,Spinner spinner){
        switch (index){
            case 1:
                arrayList = new ArrayList<>();
                arrayList.add(new spinner("Tuần"));
                arrayList.add(new spinner("Ngày"));
                arrayList.add(new spinner("Tháng"));
                arrayList.add(new spinner("Năm"));
                adapter = new CustomAdaperSpinner(arrayList);
                spinner.setAdapter(adapter);
                break;
            case 2:
                arrayList = new ArrayList<>();
                arrayList.add(new spinner("Từ 19-26/09/2022"));
                arrayList.add(new spinner("Từ 11-18/09/2022"));
                arrayList.add(new spinner("Từ 03-10/09/2022"));
                arrayList.add(new spinner("Từ 27-02/10/2022"));
                adapter= new CustomAdaperSpinner(arrayList);
                spinner.setAdapter(adapter);
                break;
            case 3:
                arrayList = new ArrayList<>();
                arrayList.add(new spinner("Tất cả"));
                arrayList.add(new spinner("hihi"));
                arrayList.add(new spinner("haha"));
                arrayList.add(new spinner("hoho"));
                adapter = new CustomAdaperSpinner(arrayList);
                spinner.setAdapter(adapter);
        }
    }

    public void SetApdaterRecyclerView(RecyclerView recyclerView, Context context){
        arrayListRoom = new ArrayList<Room>();
        arrayListRoom.add(new Room("Phòng Văn hóa-thông tin"));
        arrayListRoom.add(new Room("Văn phòng HĐNH-UBND huyện"));
        arrayListRoom.add(new Room("Phòng Tài chính kế hoạch"));
        arrayListRoom.add(new Room("Phòng Tư pháp"));
        adapterRoom = new AdapterRoom(context, arrayListRoom);
        recyclerView.setAdapter(adapterRoom);
    }



}
