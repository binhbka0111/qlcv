package com.binhdi0111.bka.customview_mvp.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.binhdi0111.bka.customview_mvp.Interface.InterfacePersonnel;
import com.binhdi0111.bka.customview_mvp.Object.Room;
import com.binhdi0111.bka.customview_mvp.Presenter.PersonnelPresenter;
import com.binhdi0111.bka.customview_mvp.R;

import java.util.ArrayList;

public class Personnel extends AppCompatActivity implements InterfacePersonnel {
    RecyclerView recyclerViewRoom;
    ImageView imgback;
    PersonnelPresenter personnelPresenter;
    EditText edtFilterStaff;

    @SuppressLint({"MissingInflatedId", "ResourceType"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personnel);
        imgback = (ImageView)findViewById(R.id.imgPersonnelListBack);
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        recyclerViewRoom = (RecyclerView) findViewById(R.id.recyclerViewRoom);

        personnelPresenter = new PersonnelPresenter(this);
        personnelPresenter.getList();
        personnelPresenter.SetAdapterRecyclerView(recyclerViewRoom,this);
        recyclerViewRoom.setNestedScrollingEnabled(false);
        edtFilterStaff = (EditText) findViewById(R.id.edtFilterStaff);
        
        
        edtFilterStaff.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("hihihiiihiiih", "onCreate: oke1" + edtFilterStaff.getText().toString());
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("hihihiiihiiih", "onCreate: oke2" + edtFilterStaff.getText().toString().trim());
                personnelPresenter.filterList(edtFilterStaff.getText().toString().trim());
                personnelPresenter.SetAdapterRecyclerView(recyclerViewRoom,Personnel.this);

            }

            @Override
            public void afterTextChanged(Editable editable) {
                Log.d("hihihiiihiiih", "onCreate: oke3" + edtFilterStaff.getText().toString());
            }
        });

//        setupUI(findViewById(R.id.activity_personnel));
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if(inputMethodManager.isAcceptingText()){
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);

        }
    }
    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                @SuppressLint("ClickableViewAccessibility")
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(Personnel.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }


    @Override
    public void filterList() {

    }
}