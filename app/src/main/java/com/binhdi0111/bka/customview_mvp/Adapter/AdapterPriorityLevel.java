package com.binhdi0111.bka.customview_mvp.Adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.binhdi0111.bka.customview_mvp.Object.Status;
import com.binhdi0111.bka.customview_mvp.R;

import java.util.ArrayList;

public class AdapterPriorityLevel extends BaseAdapter {
    ArrayList<Status> arrayList;

    public AdapterPriorityLevel(ArrayList<Status> arrayList) {
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return arrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View itemView = inflater.inflate(R.layout.list_priority_level,viewGroup,false);
        if (itemView != null){
            Status item = arrayList.get(i);

            //TextView
            @SuppressLint({"MissingInflatedId", "LocalSuppress"})
            TextView tvPriorityLevel = itemView.findViewById(R.id.textViewprioritylevel);
            ImageView imgPriorityLevel = itemView.findViewById(R.id.imageViewprioritylevel);
            tvPriorityLevel.setText(item.getName());
            imgPriorityLevel.setImageResource(item.getColor());
        }
        return itemView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.list_priority_level,parent,false);
        if (itemView != null){
            Status item = arrayList.get(position);

            //TextView
            @SuppressLint({"MissingInflatedId", "LocalSuppress"})
            TextView tvPriorityLevel = itemView.findViewById(R.id.textViewprioritylevel);
            ImageView imgPriorityLevel = itemView.findViewById(R.id.imageViewprioritylevel);
            tvPriorityLevel.setText(item.getName());
            imgPriorityLevel.setImageResource(item.getColor());
        }
        return super.getDropDownView(position, convertView, parent);
    }
}
