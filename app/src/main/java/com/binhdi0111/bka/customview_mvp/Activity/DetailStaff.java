package com.binhdi0111.bka.customview_mvp.Activity;

import android.animation.LayoutTransition;
import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.binhdi0111.bka.customview_mvp.Interface.InterfaceDetailStaff;
import com.binhdi0111.bka.customview_mvp.Presenter.DetailStaffPresenter;
import com.binhdi0111.bka.customview_mvp.R;
import com.github.mikephil.charting.charts.PieChart;

public class DetailStaff extends AppCompatActivity implements InterfaceDetailStaff {
    View view1,view2;
    TextView txtDuocGiao,txtDaGiao;
    ConstraintLayout layout1,layout2;
    Spinner spinner5,spinner1,spinner2,spinner3,spinner4;
    PieChart pieChart;
    ImageView imgBack,imgDrop;
    LinearLayout linearLayoutMyJob;
    RecyclerView recyclerView;
    LinearLayout lnStaffTitle,lnExpand;
    DetailStaffPresenter detailStaffPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_staff);
        initView();
        detailStaffPresenter = new DetailStaffPresenter(this);
        txtDuocGiao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                detailStaffPresenter.onClickChangeLayout(0);
            }
        });

        txtDaGiao.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View view) {
                detailStaffPresenter.onClickChangeLayout(1);
            }
        });
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        detailStaffPresenter.setUpPieChart(pieChart);
        detailStaffPresenter.SetAdapterRecyclerView(recyclerView,this);
        recyclerView.setNestedScrollingEnabled(false);
        linearLayoutMyJob.setNestedScrollingEnabled(false);
        detailStaffPresenter.SetAdapterSpinner(1,spinner1);
        detailStaffPresenter.SetAdapterSpinner(2,spinner2);
        detailStaffPresenter.SetAdapterSpinner(3,spinner3);
        detailStaffPresenter.SetAdapterSpinner(4,spinner4);
        detailStaffPresenter.SetAdapterSpinner(5,spinner5);


    }
    public void initView(){
        pieChart = (PieChart) findViewById(R.id.piechartStaff);
        txtDuocGiao = (TextView) findViewById(R.id.txtDcGiao);
        view1 = (View)findViewById(R.id.viewStaff1);
        txtDaGiao = (TextView) findViewById(R.id.txtDaGiao);
        view2 = (View) findViewById(R.id.viewStaff2);
        layout1 = (ConstraintLayout)findViewById(R.id.layoutdcgiao);
        layout2 = (ConstraintLayout)findViewById(R.id.layoudagiao);
        lnStaffTitle = (LinearLayout)findViewById(R.id.LinearLayoutLayout_StaffTitle);
        lnStaffTitle.getLayoutTransition().enableTransitionType(LayoutTransition.CHANGING);
        lnExpand = (LinearLayout)findViewById(R.id.LinearLayoutStaffRecy);
        imgDrop = (ImageView)findViewById(R.id.imageViewDropStaff);
        imgBack = (ImageView)findViewById(R.id.StaffBack);
        this.spinner1 = (Spinner)findViewById(R.id.SpinnerStaff1);
        this.spinner2 = (Spinner) findViewById(R.id.SpinnerStaff2);
        this.spinner3 = (Spinner) findViewById(R.id.SpinnerStaff3);
        this.spinner4 = (Spinner) findViewById(R.id.SpinnerStaff4);
        this.spinner5 = (Spinner) findViewById(R.id.SpinnerStaff5);
        linearLayoutMyJob  = (LinearLayout) findViewById(R.id.linearLayoutMyJobStaff);
        recyclerView  =(RecyclerView) findViewById(R.id.recyclerViewJobStaff);

    }

    @Override
    public void SelectTab0() {
        view1.setVisibility(View.VISIBLE);
        view2.setVisibility(View.INVISIBLE);
        txtDuocGiao.setTypeface(null, Typeface.BOLD);
        txtDaGiao.setTypeface(null,Typeface.NORMAL);
        layout1.setBackground(getResources().getDrawable(R.drawable.custom1));
        layout2.setBackground(getResources().getDrawable(R.drawable.custom2));
    }

    @Override
    public void SelectTab1() {
        view2.setVisibility(View.VISIBLE);
        view1.setVisibility(View.INVISIBLE);
        txtDaGiao.setTypeface(null, Typeface.BOLD);
        txtDuocGiao.setTypeface(null,Typeface.NORMAL);
        layout1.setBackground(getResources().getDrawable(R.drawable.custom4));
        layout2.setBackground(getResources().getDrawable(R.drawable.custom3));
    }
    public void expand(View view) {

        int v = (lnExpand.getVisibility()== View.GONE)?View.VISIBLE: View.GONE;
        TransitionManager.beginDelayedTransition(lnStaffTitle,new AutoTransition());
        lnExpand.setVisibility(v);
        if (lnExpand.getVisibility()== View.VISIBLE){
            imgDrop.setImageResource(R.drawable.ic_baseline_arrow_drop_up_24);
        }else {
            imgDrop.setImageResource(R.drawable.ic_baseline_arrow_drop_down_24);
        }
    }
}
