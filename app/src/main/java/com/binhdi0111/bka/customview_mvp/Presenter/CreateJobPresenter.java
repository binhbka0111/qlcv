package com.binhdi0111.bka.customview_mvp.Presenter;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;


import com.binhdi0111.bka.customview_mvp.Activity.CreateJob;
import com.binhdi0111.bka.customview_mvp.Interface.InterfaceCreateJob;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;


public class CreateJobPresenter {
    InterfaceCreateJob interfaceCreateJob;
    int gio,phut;
    int REQUEST_CODE_FILE =123;
    Calendar calendar;
    private SimpleDateFormat simpleDateFormat;
    ArrayAdapter adapter;
    ArrayList<String> arrayList;

    public CreateJobPresenter(InterfaceCreateJob interfaceCreateJob) {
        this.interfaceCreateJob = interfaceCreateJob;
    }

    public int RequestCode(){
        return REQUEST_CODE_FILE;
    }
    public void DatePicker(EditText editText){
        calendar = Calendar.getInstance();
        int ngay = calendar.get(Calendar.DATE);
        int thang = calendar.get(Calendar.MONTH);
        int nam = calendar.get(Calendar.YEAR);
        DatePickerDialog datePickerDialog = new DatePickerDialog(adapter.getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                calendar.set(i,i1,i2);
                simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                editText.setText(simpleDateFormat.format(calendar.getTime()));
            }
        },nam,thang,ngay);
        datePickerDialog.show();
    }
    public void TimePicker(EditText editText){
        TimePickerDialog timePickerDialog = new TimePickerDialog(adapter.getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                gio =i;
                phut = i1;
                editText.setText(String.format(Locale.getDefault(),"%02d:%02d",gio,phut));
            }
        },gio,phut,true);
        timePickerDialog.show();
    }

    public void SetAdapterSpiner(int index,Spinner spinner){
        switch (index){
            case 1:
                arrayList = new ArrayList<>();
                arrayList.add("Chọn nhóm công việc");
                arrayList.add("hihi");
                arrayList.add("haha");
                arrayList.add("hehehehehe");
                adapter = new ArrayAdapter(spinner.getContext(), android.R.layout.simple_spinner_item,arrayList);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);
                break;
            case 2:
                arrayList = new ArrayList<>();
                arrayList.add("Mã/số hiệu văn bản");
                arrayList.add("hihi");
                arrayList.add("haha");
                arrayList.add("hehehehehe");
                adapter = new ArrayAdapter(spinner.getContext(), android.R.layout.simple_spinner_item,arrayList);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);

        }
    }


}
