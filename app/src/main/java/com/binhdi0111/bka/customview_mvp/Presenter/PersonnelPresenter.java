package com.binhdi0111.bka.customview_mvp.Presenter;

import android.content.Context;
import android.util.Log;

import androidx.recyclerview.widget.RecyclerView;

import com.binhdi0111.bka.customview_mvp.Adapter.AdapterRoomStaff;
import com.binhdi0111.bka.customview_mvp.Interface.InterfacePersonnel;
import com.binhdi0111.bka.customview_mvp.Object.Room;

import java.util.ArrayList;
import java.util.Objects;

public class PersonnelPresenter {
    InterfacePersonnel interfacePersonnel;
    AdapterRoomStaff adapterRoomStaff;
    ArrayList<Room> arrayListRoom;
    ArrayList<Room> arrayListRoomCopy;

    public PersonnelPresenter(InterfacePersonnel interfacePersonnel) {
        this.interfacePersonnel = interfacePersonnel;
    }


    public void getList(){
        arrayListRoom = new ArrayList<>();
        arrayListRoomCopy = new ArrayList<>();
        arrayListRoom.add(new Room("oke"));
        arrayListRoom.add(new Room("hehe"));
        arrayListRoom.add(new Room("haha"));
        arrayListRoom.add(new Room("hihi"));
        arrayListRoom.add(new Room("huhu"));
        arrayListRoom.add(new Room("oke nhé"));
        for (int i = 0; i < arrayListRoom.size(); i++) {
            arrayListRoomCopy.add(arrayListRoom.get(i));
        }
    }
    public void SetAdapterRecyclerView(RecyclerView recyclerView, Context context){
        adapterRoomStaff = new AdapterRoomStaff(context,arrayListRoom);
        recyclerView.setAdapter(adapterRoomStaff);
    }




    public void filterList(String text){
        Log.d("ooooooooooooooooooo", "filterList: "+ arrayListRoom.size());
        Log.d("ooooooooooooooooooo", "filterListCopy: "+ arrayListRoomCopy.size());
        arrayListRoom.clear();
        for (int i = 0; i < arrayListRoomCopy.size(); i++) {
            if ( arrayListRoomCopy.get(i).name.toLowerCase().trim().contains(text.toLowerCase())) {
                arrayListRoom.add(arrayListRoomCopy.get(i));
            }
        }
        Log.d("ooooooooooooooooooo", "filterList1: "+ arrayListRoom.size());
        Log.d("ooooooooooooooooooo", "filterListCopy2: "+ arrayListRoomCopy.size());
    }







}
