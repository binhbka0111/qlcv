package com.binhdi0111.bka.customview_mvp.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;

import com.binhdi0111.bka.customview_mvp.Fragment.Fragment_Dashboard;
import com.binhdi0111.bka.customview_mvp.Fragment.Fragment_Job;
import com.binhdi0111.bka.customview_mvp.Fragment.Fragment_Notify;
import com.binhdi0111.bka.customview_mvp.Fragment.Fragment_User;
import com.binhdi0111.bka.customview_mvp.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

public class MainActivity extends AppCompatActivity {
    BottomNavigationView bottomNavigationView;
    ImageView imgNotyfi;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Fragment_Dashboard fragment = new Fragment_Dashboard();
        transaction.add(R.id.framelayout, fragment);
        transaction.commit();
        bottomNavigationView =(BottomNavigationView) findViewById(R.id.bottomNavigation);
        bottomNavigationView.setItemIconTintList(null);
        bottomNavigationView.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment;
                switch (item.getItemId()){
                    case (R.id.menu2):
                        fragment = new Fragment_Job();
                        loadFragment(fragment);
                        return true;
                    case (R.id.menu3):
                        fragment = new Fragment_Notify();
                        loadFragment(fragment);
                        return true;
                    case (R.id.menu4):
                        fragment = new Fragment_User();
                        loadFragment(fragment);
                        return true;
                    default:
                        fragment = new Fragment_Dashboard();
                        loadFragment(fragment);
                        return true;
                }
            }
        });
        Intent i = getIntent();
        String data = i.getStringExtra("imgNotify");
        if (data != null && data.contentEquals("1")) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.framelayout, new Fragment_Notify());
            fragmentTransaction.commitNow();
            bottomNavigationView.setSelectedItemId(R.id.menu3);
        }

        Intent myjob = getIntent();
        String result = myjob.getStringExtra("layoutMyJob");
        if (result != null && result.contentEquals("1")) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.framelayout, new Fragment_Job());
            fragmentTransaction.commitNow();
            bottomNavigationView.setSelectedItemId(R.id.menu2);
        }

    }
    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.framelayout, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}