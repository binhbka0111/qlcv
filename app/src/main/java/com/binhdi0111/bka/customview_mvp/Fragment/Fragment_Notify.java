package com.binhdi0111.bka.customview_mvp.Fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.binhdi0111.bka.customview_mvp.Interface.InterfaceNotify;
import com.binhdi0111.bka.customview_mvp.Presenter.NotifyPresenter;
import com.binhdi0111.bka.customview_mvp.R;

public class Fragment_Notify extends Fragment implements InterfaceNotify {
    View view1,view2;
    TextView txtTatca,txtChuaDoc;
    LinearLayout layout1,layout2;
    NotifyPresenter notifyPresenter;
    RecyclerView recyclerViewNew,recyclerViewOld;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notify,container,false);
        initView(view);
        notifyPresenter = new NotifyPresenter(this);
        layout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notifyPresenter.onClickChangeLayout(0);
            }
        });
        layout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notifyPresenter.onClickChangeLayout(1);
            }
        });
        notifyPresenter.SetRecyclerViewNew(recyclerViewNew,getActivity());
        notifyPresenter.SetRecyclerViewOld(recyclerViewOld,getActivity());
        return view;
    }

    private void initView(View view){
        txtTatca = (TextView)view.findViewById(R.id.textViewTatCa);
        txtChuaDoc = (TextView)view.findViewById(R.id.textViewChuaDoc);
        view1 = (View)view.findViewById(R.id.ViewC);
        view2= (View)view.findViewById(R.id.ViewD);
        layout1 = view.findViewById(R.id.tatca);
        layout2 = view.findViewById(R.id.chuadoc);
        recyclerViewNew = (RecyclerView) view.findViewById(R.id.recyclerViewNew);
        recyclerViewOld =(RecyclerView)view.findViewById(R.id.recyclerViewOld);

    }

    @Override
    public void SelectTab0() {
        view1.setVisibility(View.VISIBLE);
        view2.setVisibility(View.INVISIBLE);
        txtTatca.setTextColor(Color.rgb(16,104,178));
        txtChuaDoc.setTextColor(Color.BLACK);
        layout2.setBackgroundColor(Color.rgb(238,235,235));
        layout1.setBackgroundColor(Color.rgb(228,238,247));
    }

    @Override
    public void SelectTab1() {
        view2.setVisibility(View.VISIBLE);
        view1.setVisibility(View.INVISIBLE);
        txtChuaDoc.setTextColor(Color.rgb(16,104,178));
        txtTatca.setTextColor(Color.BLACK);
        layout1.setBackgroundColor(Color.rgb(238,235,235));
        layout2.setBackgroundColor(Color.rgb(228,238,247));
    }
}
