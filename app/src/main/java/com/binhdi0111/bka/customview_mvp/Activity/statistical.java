package com.binhdi0111.bka.customview_mvp.Activity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.binhdi0111.bka.customview_mvp.Interface.InterfaceStatistical;
import com.binhdi0111.bka.customview_mvp.Presenter.statisticalPresenter;
import com.binhdi0111.bka.customview_mvp.R;
import com.github.mikephil.charting.charts.PieChart;

public class statistical extends AppCompatActivity implements InterfaceStatistical {
    LinearLayout lnDuocGiao, lnDaGiao;
    PieChart pieChart;
    View view1,view2;
    TextView txtDcGiao,txtDaGiao;
    ImageView imgBack;
    com.binhdi0111.bka.customview_mvp.Presenter.statisticalPresenter statisticalPresenter;
    Spinner spinner1,spinner2,spinner3,spinner4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistical);
        initView();
        statisticalPresenter = new statisticalPresenter(this);
        lnDuocGiao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                statisticalPresenter.onClickChangeLayout(0);
            }
        });
        lnDaGiao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                statisticalPresenter.onClickChangeLayout(1);
            }
        });
        statisticalPresenter.PieChart(pieChart);
        statisticalPresenter.SetAdapterSpinner(1,spinner1);
        statisticalPresenter.SetAdapterSpinner(2,spinner2);
        statisticalPresenter.SetAdapterSpinner(3,spinner3);
        statisticalPresenter.SetAdapterSpinner(4,spinner4);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }


    public void initView(){
        imgBack = (ImageView) findViewById(R.id.imgback2);
        pieChart=(PieChart)findViewById(R.id.piechart3);
        lnDaGiao = (LinearLayout) findViewById(R.id.lnviecdagiao);
        lnDuocGiao = (LinearLayout) findViewById(R.id.lnviecdcgiao);
        view1 =(View) findViewById(R.id.viewdcgiao);
        view2 =(View) findViewById(R.id.viewdagiao);
        txtDcGiao = (TextView) findViewById(R.id.txtviecduocgiao);
        txtDaGiao =(TextView) findViewById(R.id.txtviecdagiao);
        spinner1 = findViewById(R.id.SpinerThongke1);
        spinner2 = findViewById(R.id.SpinerThongke2);
        spinner3 = findViewById(R.id.SpinerThongke3);
        spinner4 = findViewById(R.id.SpinerThongke4);
    }

    @Override
    public void SelectTab0() {
        view1.setVisibility(View.VISIBLE);
        view2.setVisibility(View.INVISIBLE);
        txtDcGiao.setTypeface(null, Typeface.BOLD);
        txtDaGiao.setTypeface(null,Typeface.NORMAL);
        lnDuocGiao.setBackground(getResources().getDrawable(R.drawable.custom1));
        lnDaGiao.setBackground(getResources().getDrawable(R.drawable.custom2));
    }

    @Override
    public void SelectTab1() {
        view2.setVisibility(View.VISIBLE);
        view1.setVisibility(View.INVISIBLE);
        txtDaGiao.setTypeface(null, Typeface.BOLD);
        txtDcGiao.setTypeface(null,Typeface.NORMAL);
        lnDuocGiao.setBackground(getResources().getDrawable(R.drawable.custom4));
        lnDaGiao.setBackground(getResources().getDrawable(R.drawable.custom3));
    }
}
