package com.binhdi0111.bka.customview_mvp;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import java.util.Calendar;
import java.util.Date;

public class Myservice extends Service {
    private static final String CHANNEL_ID = "binhbka0111";
    Calendar calendar;


    @Override
    public void onCreate() {
        createNotificationChannel();
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sendNotification();
        return super.onStartCommand(intent, flags, startId);
    }

    private void sendNotification(){
        String title = "Tạo công việc mới";
        calendar = Calendar.getInstance();
        int ngay = calendar.get(Calendar.DATE);
        int thang = calendar.get(Calendar.MONTH);
        int nam = calendar.get(Calendar.YEAR);
        int phut = calendar.get(Calendar.MINUTE);
        int gio = calendar.get(Calendar.HOUR);
        String message ="Đặng Văn Bình đã tạo công việc lúc"+gio +":"+phut+"/"+ngay+"/"+thang+"/"+nam;
        String time =   ""+gio +":"+phut+"/"+ngay+"/"+thang+"/"+nam ;
        Intent intent = new Intent("send-notify_data");
        intent.putExtra("title",title);
        intent.putExtra("message",message);
        intent.putExtra("time",time);
        sendBroadcast(intent);
        Notification notification = new NotificationCompat.Builder(this,CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setAutoCancel(true)
                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if(notificationManager != null){
            notificationManager.notify(getNotificationId(),notification);
        }
        startForeground(1,notification);

    }
    public int getNotificationId(){
        return (int) new Date().getTime();
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}
