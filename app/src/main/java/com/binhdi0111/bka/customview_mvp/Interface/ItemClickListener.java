package com.binhdi0111.bka.customview_mvp.Interface;

import android.view.View;

public interface ItemClickListener {
    void onClick(View view, int adapterPosition);
}
