package com.binhdi0111.bka.customview_mvp.Presenter;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.binhdi0111.bka.customview_mvp.Adapter.AdapterPriorityLevel;
import com.binhdi0111.bka.customview_mvp.Adapter.CustomAdaperSpinner;
import com.binhdi0111.bka.customview_mvp.Interface.IntefacePriorityLevel;
import com.binhdi0111.bka.customview_mvp.Object.Status;
import com.binhdi0111.bka.customview_mvp.Object.spinner;
import com.binhdi0111.bka.customview_mvp.R;

import java.util.ArrayList;

public class PriorityLevelPresenter {
    IntefacePriorityLevel intefacePriorityLevel;
    ArrayList<Status> arrayList;
    AdapterPriorityLevel adapterPriorityLevel;
    String result;
    int colorPriority;
    public PriorityLevelPresenter(IntefacePriorityLevel intefacePriorityLevel) {
        this.intefacePriorityLevel = intefacePriorityLevel;
    }

    public void SetAdapterSpinner(Spinner spinner){
        arrayList = new ArrayList<>();
        arrayList.add(new Status("Chưa chọn mức độ ưu tiên", R.drawable.ic_baseline_circle_24_1));
        arrayList.add(new Status("Mức 1", R.drawable.ic_baseline_circle_24_red));
        arrayList.add(new Status("Mức 2", R.drawable.ic_baseline_circle_24_yellow));
        arrayList.add(new Status("Mức 3", R.drawable.ic_baseline_circle_24_2));
        adapterPriorityLevel = new AdapterPriorityLevel(arrayList);
        spinner.setAdapter(adapterPriorityLevel);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                result = arrayList.get(i).getName();
                colorPriority = arrayList.get(i).getColor();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }
    public String ResultContent(){
        return result;
    }

    public int ResultColor(){
        return colorPriority;
    }

}
