package com.binhdi0111.bka.customview_mvp.Presenter;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import com.binhdi0111.bka.customview_mvp.Adapter.AdapterNewNotify;
import com.binhdi0111.bka.customview_mvp.Adapter.AdapterNotifyOld;
import com.binhdi0111.bka.customview_mvp.Interface.InterfaceNotify;
import com.binhdi0111.bka.customview_mvp.Object.Notify;

import java.util.ArrayList;
import java.util.Calendar;

public class NotifyPresenter {
    InterfaceNotify interfaceNotify;
    Calendar calendar;
    ArrayList<Notify> arrayList;
    String time;
    AdapterNewNotify adapterNewNotify ;
    AdapterNotifyOld adapterNotifyOld;


    public NotifyPresenter(InterfaceNotify interfaceNotify) {
        this.interfaceNotify = interfaceNotify;
    }

    public void onClickChangeLayout(int index){
        switch (index){
            case 0:
                interfaceNotify.SelectTab0();
                break;
            case 1:
                interfaceNotify.SelectTab1();
                break;
        }
    }
    public void SetRecyclerViewNew(RecyclerView recyclerView, Context context){
        calendar = Calendar.getInstance();
        int ngay = calendar.get(Calendar.DATE);
        int thang = calendar.get(Calendar.MONTH);
        int nam = calendar.get(Calendar.YEAR);
        int phut = calendar.get(Calendar.MINUTE);
        int gio = calendar.get(Calendar.HOUR);
        time =  ""+gio +":"+phut+"/"+ngay+"/"+thang+"/"+nam ;

        arrayList = new ArrayList<Notify>();
        arrayList.add(new Notify("Công việc vừa được cập nhật","Công việc đã quá hạn hoàn thành",time+""));
        arrayList.add(new Notify("Công việc vừa được cập nhật","Công việc đã quá hạn hoàn thành",time+""));
        arrayList.add(new Notify("Công việc vừa được cập nhật","Công việc đã quá hạn hoàn thành",time+""));
        arrayList.add(new Notify("Công việc vừa được cập nhật","Công việc đã quá hạn hoàn thành",time+""));
        arrayList.add(new Notify("Công việc vừa được cập nhật","Công việc đã quá hạn hoàn thành",time+""));
        arrayList.add(new Notify("Công việc vừa được cập nhật","Công việc đã quá hạn hoàn thành",time+""));
        arrayList.add(new Notify("Công việc vừa được cập nhật","Công việc đã quá hạn hoàn thành",time+""));
        arrayList.add(new Notify("Công việc vừa được cập nhật","Công việc đã quá hạn hoàn thành",time+""));
        arrayList.add(new Notify("Công việc vừa được cập nhật","Công việc đã quá hạn hoàn thành",time+""));
        arrayList.add(new Notify("Công việc vừa được cập nhật","Công việc đã quá hạn hoàn thành",time+""));
        arrayList.add(new Notify("Công việc vừa được cập nhật","Công việc đã quá hạn hoàn thành",time+""));
        arrayList.add(new Notify("Công việc vừa được cập nhật","Công việc đã quá hạn hoàn thành",time+""));
        adapterNewNotify = new AdapterNewNotify(context,arrayList);
        recyclerView.setAdapter(adapterNewNotify);

    }
    public void SetRecyclerViewOld(RecyclerView recyclerView,Context context){
        arrayList = new ArrayList<Notify>();
        arrayList.add(new Notify("Công việc vừa được cập nhật","Công việc đã quá hạn hoàn thành",time+""));
        arrayList.add(new Notify("Công việc vừa được cập nhật","Công việc đã quá hạn hoàn thành",time+""));
        arrayList.add(new Notify("Công việc vừa được cập nhật","Công việc đã quá hạn hoàn thành",time+""));
        arrayList.add(new Notify("Công việc vừa được cập nhật","Công việc đã quá hạn hoàn thành",time+""));
        arrayList.add(new Notify("Công việc vừa được cập nhật","Công việc đã quá hạn hoàn thành",time+""));
        arrayList.add(new Notify("Công việc vừa được cập nhật","Công việc đã quá hạn hoàn thành",time+""));
        arrayList.add(new Notify("Công việc vừa được cập nhật","Công việc đã quá hạn hoàn thành",time+""));
        arrayList.add(new Notify("Công việc vừa được cập nhật","Công việc đã quá hạn hoàn thành",time+""));
        arrayList.add(new Notify("Công việc vừa được cập nhật","Công việc đã quá hạn hoàn thành",time+""));
        arrayList.add(new Notify("Công việc vừa được cập nhật","Công việc đã quá hạn hoàn thành",time+""));
        adapterNotifyOld = new AdapterNotifyOld(context,arrayList);
        recyclerView.setAdapter(adapterNotifyOld);
    }
}
