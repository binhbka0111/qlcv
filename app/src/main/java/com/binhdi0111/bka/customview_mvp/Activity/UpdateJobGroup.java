package com.binhdi0111.bka.customview_mvp.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.binhdi0111.bka.customview_mvp.BottomSheetDialogFragment.BottomSheetConfirmJob;
import com.binhdi0111.bka.customview_mvp.Interface.InterfaceUpdateJobGroup;
import com.binhdi0111.bka.customview_mvp.Presenter.UpdateJobGroupPresenter;
import com.binhdi0111.bka.customview_mvp.R;

public class UpdateJobGroup extends AppCompatActivity implements InterfaceUpdateJobGroup {
    BottomSheetConfirmJob bottomSheetConfirmJob;
    Spinner spinnerJobGroup;
    Button btnSave,btnCancel;
    UpdateJobGroupPresenter updateJobGroupPresenter;
    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_job_group);
        updateJobGroupPresenter = new UpdateJobGroupPresenter(this);
        spinnerJobGroup = (Spinner) findViewById(R.id.SpinnerJobGroup);
        btnCancel = (Button) findViewById(R.id.buttonCancelJobGroup);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        updateJobGroupPresenter.SetAdapterSpinner(spinnerJobGroup);
        btnSave = (Button)findViewById(R.id.buttonSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetConfirmJob = BottomSheetConfirmJob.newInstance(new BottomSheetConfirmJob.DialogUpdate() {
                    @Override
                    public void clicklistenerUpdateJobGroup(String content) {
                        Intent sendcontent = new Intent("send_content_jobgroup_data");
                        sendcontent.putExtra("sendcontent",content);
                        Log.d("ppppppppppp", "clicklistenerUpdateJobGroup: "+content);
                        sendBroadcast(sendcontent);
                        onBackPressed();
                    }
                    @Override
                    public void clickListenerPriorityLevel(String content, int color) {

                    }
                });
                Bundle bundle = new Bundle();
                bundle.putString("content",updateJobGroupPresenter.SetAdapterSpinner(spinnerJobGroup));
                bundle.putString("title","Bạn có muốn cập nhật nhóm công việc cho công việc hiện tai?");
                bottomSheetConfirmJob.setArguments(bundle);
                bottomSheetConfirmJob.show(getSupportFragmentManager(), bottomSheetConfirmJob.getTag());
            }
        });

    }


}
