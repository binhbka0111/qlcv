package com.binhdi0111.bka.customview_mvp.BottomSheetDialogFragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.binhdi0111.bka.customview_mvp.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class BottomSheetDialogFragmentDate extends BottomSheetDialogFragment {

    static BottomSheetDialogFragmentDate bottomSheetDialogFragmentDate;
    static DialogDateJob listener;
    TextView txtLabel,txtTitle;
    EditText edtDate,edtTime;
    ImageView imgDate,imgTime;
    LinearLayout lnCancel,lnConfirm;
    Calendar calendar;
    SimpleDateFormat simpleDateFormat;
    int gio,phut;

    public BottomSheetDialogFragmentDate(DialogDateJob listener) {
        this.listener = listener;
    }

    public static BottomSheetDialogFragmentDate newInstance(DialogDateJob listener) {
        bottomSheetDialogFragmentDate = new BottomSheetDialogFragmentDate(listener);
        return bottomSheetDialogFragmentDate;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(bottomSheetDialogFragmentDate.STYLE_NORMAL, R.style.DialogStyle);
    }

    @SuppressLint("MissingInflatedId")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottom_sheet_dialog_fragment_date,container,false);
        txtLabel = (TextView) view.findViewById(R.id.detailJobLabelDate);
        txtTitle = (TextView) view.findViewById(R.id.textViewTitleDate);
        edtDate = (EditText) view.findViewById(R.id.editTextDateDialog);
        imgDate = (ImageView) view.findViewById(R.id.imageViewDateDialog);
        imgTime = (ImageView) view.findViewById(R.id.imageViewTimeDialog);
        edtTime = (EditText) view.findViewById(R.id.editTextTimeDialog);
        lnConfirm = (LinearLayout) view.findViewById(R.id.linearLayoutConfirm);
        lnCancel = (LinearLayout) view.findViewById(R.id.linearLayoutCancel);
        imgTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePicker(edtTime);
            }
        });
        imgDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePicker(edtDate);
            }
        });
        lnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String time = edtTime.getText().toString().trim();
                String date = edtDate.getText().toString().trim();
                listener.clickListenerDate(date,time);
                bottomSheetDialogFragmentDate.dismiss();
            }
        });
        lnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogFragmentDate.dismiss();
            }
        });
        Bundle bundle = this.getArguments();
        String label = bundle.getString("label");
        String title = bundle.getString("title");
        txtLabel.setText(label);
        txtTitle.setText(title);
        return view;
    }
    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        View contentView = View.inflate(getContext(), R.layout.bottom_sheet_dialog_fragment_date, null);
        dialog.setContentView(contentView);
        ((View) contentView.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
    }
    public interface DialogDateJob {
        public void clickListenerDate(String date, String time);
    }
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (DialogDateJob) getActivity();
        }
        catch (ClassCastException e) {
            Log.e("000000000000", "onAttach: ClassCastException: "
                    + e.getMessage());
        }
    }
    private void DatePicker(EditText editText){
        calendar = Calendar.getInstance();
        int ngay = calendar.get(Calendar.DATE);
        int thang = calendar.get(Calendar.MONTH);
        int nam = calendar.get(Calendar.YEAR);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                calendar.set(i,i1,i2);
                simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                editText.setText(simpleDateFormat.format(calendar.getTime()));
            }
        },nam,thang,ngay);
        datePickerDialog.show();
    }
    private void TimePicker(EditText editText){
        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                gio =i;
                phut = i1;
                editText.setText(String.format(Locale.getDefault(),"%02d:%02d",gio,phut));
            }
        },gio,phut,true);
        timePickerDialog.show();
    }
}
