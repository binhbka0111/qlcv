package com.binhdi0111.bka.customview_mvp.Object;

public class JobStatus {
    String title;
    String status;
    String name;
    String time;
    int color;

    public JobStatus(String title, String status, String name, String time, int color) {
        this.title = title;
        this.status = status;
        this.name = name;
        this.time = time;
        this.color = color;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int
    getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
