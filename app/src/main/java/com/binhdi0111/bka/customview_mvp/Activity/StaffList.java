package com.binhdi0111.bka.customview_mvp.Activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.binhdi0111.bka.customview_mvp.Interface.InterfaceStaffList;
import com.binhdi0111.bka.customview_mvp.Presenter.StaffListPresenter;
import com.binhdi0111.bka.customview_mvp.R;

public class StaffList extends AppCompatActivity implements InterfaceStaffList {
    RecyclerView recyclerViewRoom;
    ImageView imgBack;
    StaffListPresenter staffListPresenter;


    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_list);
        recyclerViewRoom = (RecyclerView) findViewById(R.id.recyclerViewRoomStaff);
        imgBack = (ImageView)findViewById(R.id.StaffListBack);


        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        staffListPresenter = new StaffListPresenter(this);
        staffListPresenter.setRecyclerStaffList(recyclerViewRoom,this);
    }
}
