package com.binhdi0111.bka.customview_mvp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.binhdi0111.bka.customview_mvp.Activity.DetailStaff;
import com.binhdi0111.bka.customview_mvp.R;
import com.binhdi0111.bka.customview_mvp.Object.Room;
import com.binhdi0111.bka.customview_mvp.Object.Staff;

import java.util.ArrayList;

public class AdapterRoomStaff extends RecyclerView.Adapter<AdapterRoomStaff.StaffViewHolder>{
    Context context;
    ArrayList<Room> arrayList;



    public AdapterRoomStaff(Context context, ArrayList<Room> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public StaffViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_staff,parent,false);
        return new StaffViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StaffViewHolder holder, int position) {
        Room room = arrayList.get(position);
        holder.txtRoom.setText(room.getName());
        boolean isVisibility= room.visibility;
        holder.linearLayoutExpandable.setVisibility(isVisibility ?View.VISIBLE :View.GONE );
        holder.arrayListStaff = new ArrayList<ArrayList<Staff>>();
        for(int i = 0 ; i< arrayList.size();i++){
            ArrayList<Staff> staffArrayList = new ArrayList<>();
            for(int j = 0 ; j< position +1 ;j++){
                staffArrayList.add(new Staff("Đặng Văn Bình","Chủ tịch",R.drawable.ars));
            }
            holder.arrayListStaff.add(staffArrayList);
        }
        holder.txtCountStaff.setText(" ("+holder.arrayListStaff.get(position).size()+")");

        holder.adapterStaffChild = new AdapterStaffChild(context, holder.arrayListStaff.get(position), new AdapterStaffChild.ItemClick() {
            @Override
            public void onClickItem(int index) {
                Intent intent = new Intent(context, DetailStaff.class);
                context.startActivity(intent);
            }
        });
        holder.recyclerViewStaffChild.setAdapter(holder.adapterStaffChild);
        holder.recyclerViewStaffChild.setNestedScrollingEnabled(false);
        if (!room.isVisibility()){
            holder.imgViewDrop.setImageResource(R.drawable.ic_baseline_arrow_drop_down_24);
        }else {
            holder.imgViewDrop.setImageResource(R.drawable.ic_baseline_arrow_drop_up_24);
        }



    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class StaffViewHolder extends RecyclerView.ViewHolder {
        TextView txtRoom,txtCountStaff;
        ImageView imgViewDrop;
        LinearLayout linearLayoutClick,linearLayoutExpandable;
        AdapterStaffChild adapterStaffChild;
        ArrayList<ArrayList<Staff>> arrayListStaff;
        RecyclerView recyclerViewStaffChild;




        public StaffViewHolder(@NonNull View itemView) {
            super(itemView);
            txtRoom = (TextView)itemView.findViewById(R.id.textViewRoom) ;
            txtCountStaff = (TextView)itemView.findViewById(R.id.txtCountStaff) ;
            linearLayoutClick = (LinearLayout) itemView.findViewById(R.id.LinearLayoutLayout_StaffTitle);
            linearLayoutExpandable =(LinearLayout) itemView.findViewById(R.id.LinearLayoutStaffRecy);
            linearLayoutClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Room room = arrayList.get(getAdapterPosition());
                    room.setVisibility(!room.isVisibility());
                    notifyItemChanged(getAdapterPosition());
                }
            });
            recyclerViewStaffChild = (RecyclerView) itemView.findViewById(R.id.recyclerViewStaffChild);
            imgViewDrop = (ImageView) itemView.findViewById(R.id.imageViewDrop);



        }
    }
}
