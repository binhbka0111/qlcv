package com.binhdi0111.bka.customview_mvp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.binhdi0111.bka.customview_mvp.Activity.MainActivity;
import com.binhdi0111.bka.customview_mvp.Object.Notify;
import com.binhdi0111.bka.customview_mvp.R;

import java.util.ArrayList;

public class AdapterNewNotify extends RecyclerView.Adapter<AdapterNewNotify.ViewHolder> {
    MainActivity context;
    ArrayList<Notify> arrayList;

    public AdapterNewNotify(Context context, ArrayList<Notify> arrayList) {
        this.context = (MainActivity) context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_notify,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Notify notify = arrayList.get(position);
        holder.txtTitle.setText(notify.getTitle());
        holder.txtMessage.setText(notify.getMessage());
        holder.txtTime.setText(notify.getTime());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtTitle,txtMessage,txtTime;
        ImageView imgTrangThai,imgTrangthai1;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.titlethienthiMoi);
            txtMessage = (TextView) itemView.findViewById(R.id.contenthienthiMoi);
            txtTime= (TextView) itemView.findViewById(R.id.thoigianhienthiMoi);
            imgTrangThai = (ImageView)itemView.findViewById(R.id.imgthongbaomoi);
            imgTrangthai1 = (ImageView)itemView.findViewById(R.id.imageView32);

        }
    }
}
