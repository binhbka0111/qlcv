package com.binhdi0111.bka.customview_mvp.Adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.binhdi0111.bka.customview_mvp.R;
import com.binhdi0111.bka.customview_mvp.Object.Status;

import java.util.ArrayList;

public class CustomAdapter extends BaseAdapter {
    ArrayList<Status> data;

    public CustomAdapter(ArrayList<Status> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflate = LayoutInflater.from(viewGroup.getContext());
        View itemView = inflate.inflate(R.layout.list_spinner, viewGroup, false);
        if (itemView != null){
           Status item = data.get(i);

           //TextView
           @SuppressLint({"MissingInflatedId", "LocalSuppress"})
           TextView tvName = itemView.findViewById(R.id.textViewSpinner);
           tvName.setText(item.getName());
           tvName.setTextColor(item.getColor());
        }
        return itemView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflate = LayoutInflater.from(parent.getContext());
        View itemView = inflate.inflate(R.layout.list_spinner, parent, false);
        if (itemView != null){
            Status item = data.get(position);

            //TextView
            @SuppressLint({"MissingInflatedId", "LocalSuppress"})
            TextView tvName = itemView.findViewById(R.id.textViewSpinner);
            tvName.setText(item.getName());
            tvName.setTextColor(item.getColor());

        }
        return super.getDropDownView(position, convertView, parent);
    }
}
