package com.binhdi0111.bka.customview_mvp.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.binhdi0111.bka.customview_mvp.Object.Item;
import com.binhdi0111.bka.customview_mvp.R;

import java.util.ArrayList;

public class AdapterUser extends RecyclerView.Adapter<AdapterUser.ViewHolder> {
    Context context;
    ItemClick itemClick;
    ArrayList<Item> arrayList;

    public AdapterUser(Context context, ItemClick itemClick, ArrayList<Item> arrayList) {
        this.context = context;
        this.itemClick = itemClick;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_user,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Item item = arrayList.get(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClick.onClickItem(holder.getAdapterPosition());
            }
        });
        holder.txtItem.setText(item.getName());
        switch (arrayList.get(position).getIndex()){
            case 1: holder.txtItem.setPaintFlags(holder.txtItem.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
                    holder.txtItem.setTextColor(Color.rgb(16,104,178));
                    break;
            default: break;
        }

        holder.imgItem.setImageResource(item.getImage());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgItem;
        TextView txtItem;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtItem =(TextView) itemView.findViewById(R.id.textView33);
            imgItem = (ImageView) itemView.findViewById(R.id.imageView31);
        }
    }


    public  interface ItemClick{
        void onClickItem(int index);
    }

}
