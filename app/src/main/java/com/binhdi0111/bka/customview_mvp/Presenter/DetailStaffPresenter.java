package com.binhdi0111.bka.customview_mvp.Presenter;

import android.content.Context;
import android.graphics.Color;
import android.widget.Spinner;

import androidx.recyclerview.widget.RecyclerView;

import com.binhdi0111.bka.customview_mvp.Adapter.AdapterJob;
import com.binhdi0111.bka.customview_mvp.Adapter.CustomAdaperSpinner;
import com.binhdi0111.bka.customview_mvp.Adapter.CustomAdapter;
import com.binhdi0111.bka.customview_mvp.Activity.DetailStaff;
import com.binhdi0111.bka.customview_mvp.Interface.InterfaceDetailStaff;
import com.binhdi0111.bka.customview_mvp.Object.JobStatus;
import com.binhdi0111.bka.customview_mvp.Object.Status;
import com.binhdi0111.bka.customview_mvp.Object.spinner;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class DetailStaffPresenter {
    InterfaceDetailStaff interfaceDetailStaff;
    DetailStaff context;
    ArrayList<spinner> arrayList;
    CustomAdapter adapterColor;
    CustomAdaperSpinner adapter;
    ArrayList<Status> arrayListStatus;
    AdapterJob adapterJob;
    ArrayList<JobStatus> arrayListJob;
    String time;
    Calendar calendar;
    SimpleDateFormat simpleDateFormat;
    public DetailStaffPresenter(InterfaceDetailStaff interfaceDetailStaff) {
        this.interfaceDetailStaff = interfaceDetailStaff;
    }
    public void onClickChangeLayout(int index){
        switch (index){
            case 0:
                interfaceDetailStaff.SelectTab0();
                break;
            case 1:
                interfaceDetailStaff.SelectTab1();
                break;
        }
    }
    public void setUpPieChart(PieChart pieChart) {
        ArrayList<PieEntry> yEntrys = new ArrayList<>();
        ArrayList<String> xEntrys = new ArrayList<>();
        float[] yData = { 10, 20, 30,30 ,10};
        String[] xData = { "", "", "","","" };

        for (int i = 0; i < yData.length;i++){
            yEntrys.add(new PieEntry(yData[i],i));
        }
        for (int i = 0; i < xData.length;i++){
            xEntrys.add(xData[i]);
        }

        PieDataSet pieDataSet=new PieDataSet(yEntrys,"");
        pieDataSet.setSliceSpace(2);
        pieDataSet.setValueTextSize(12);

        ArrayList<Integer> colors=new ArrayList<>();
        colors.add(Color.rgb(238,235,235));
        colors.add(Color.rgb(76,166,65));
        colors.add(Color.rgb(236,156,82));
        colors.add(Color.rgb(58,119,252));
        colors.add(Color.rgb(226,93,91));

        pieDataSet.setColors(colors);
        Legend legend=pieChart.getLegend();
        legend.setForm(Legend.LegendForm.CIRCLE);
        legend.setPosition(Legend.LegendPosition.LEFT_OF_CHART);

        PieData pieData=new PieData(pieDataSet);
        pieData.setDrawValues(false);
        pieChart.setData(pieData);
        pieChart.invalidate();
        pieChart.setRotationEnabled(true);
        pieChart.setDescription(new Description());
        pieChart.setHoleRadius(35f);
        pieChart.setTransparentCircleAlpha(0);
        pieChart.setCenterText("Tổng\n"+10);
        pieChart.setCenterTextSize(10);
        pieChart.setDrawEntryLabels(true);
        pieChart.getLegend().setEnabled(false);
        pieChart.getDescription().setEnabled(false);
        pieChart.setDrawSliceText(false);
        float x = 70;
        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleRadius(x);
        pieChart.setOnChartValueSelectedListener((OnChartValueSelectedListener) context);
    }

    public void SetAdapterRecyclerView(RecyclerView recyclerView, Context context){
        calendar = Calendar.getInstance();
        simpleDateFormat = new SimpleDateFormat("hh:mm dd/MM/yyyy");
        time = simpleDateFormat.format(calendar.getTime());;
        arrayListJob = new ArrayList<>();
        arrayListJob.add(new JobStatus("Về việc test chức năng dự án","Hoàn thành đúng hạn","C.trì Đặng Văn Bình",time,Color.rgb(76,166,65)));
        arrayListJob.add(new JobStatus("Test màn","Chưa thực hiện","C.trì Đặng Văn Bình",time,Color.rgb(116,115,115)));
        arrayListJob.add(new JobStatus("Test thông báo","Đang thực hiện quá hạn","C.trì Đặng Văn Bình",time,Color.rgb(236,156,82)));
        arrayListJob.add(new JobStatus("test nhắc việc","Đang thực hiện trong hạn","C.trì Đặng Văn Bình",time,Color.rgb(58,119,252)));
        arrayListJob.add(new JobStatus("Công việc mới","Đã hủy","C.trì Đặng Văn Bình",time,Color.rgb(226,93,91)));
        arrayListJob.add(new JobStatus("Test gia hạn","Chờ duyệt hoàn thành","C.trì Đặng Văn Bình",time,Color.rgb(204,17,192)));
        arrayListJob.add(new JobStatus("test app","Đã hủy","C.trì Đặng Văn Bình",time,Color.rgb(226,93,91)));
        arrayListJob.add(new JobStatus("test công việc","Đã hủy","C.trì Đặng Văn Bình",time,Color.rgb(226,93,91)));
        adapterJob = new AdapterJob(context,arrayListJob);
        recyclerView.setAdapter(adapterJob);
    }
    public void SetAdapterSpinner(int index, Spinner spinner){
        switch (index){
            case 1:
                arrayList = new ArrayList<>();
                arrayList.add(new spinner("Tất cả"));
                arrayList.add(new spinner("Chủ trì"));
                arrayList.add(new spinner("Phối hợp"));
                arrayList.add(new spinner("Giám sát"));
                adapter = new CustomAdaperSpinner(arrayList);
                spinner.setAdapter(adapter);
                break;
            case 2:
                arrayList = new ArrayList<>();
                arrayList.add(new spinner("Thời gian tạo"));
                arrayList.add(new spinner("Hạn hoàn thành"));
                arrayList.add(new spinner("Ngày bắt đầu"));
                adapter= new CustomAdaperSpinner(arrayList);
                spinner.setAdapter(adapter);
                break;
            case 3:
                arrayList = new ArrayList<>();
                arrayList.add(new spinner("Tuần"));
                arrayList.add(new spinner("Ngày"));
                arrayList.add(new spinner("Tháng"));
                arrayList.add(new spinner("Năm"));
                adapter = new CustomAdaperSpinner(arrayList);
                spinner.setAdapter(adapter);
                break;
            case 4:
                arrayList = new ArrayList<>();
                arrayList.add(new spinner("Từ 19-26/09/2022"));
                arrayList.add(new spinner("Từ 11-18/09/2022"));
                arrayList.add(new spinner("Từ 03-10/09/2022"));
                arrayList.add(new spinner("Từ 27-02/10/2022"));
                adapter = new CustomAdaperSpinner(arrayList);
                spinner.setAdapter(adapter);
                break;
            case 5:
                arrayListStatus = new ArrayList<>();
                arrayListStatus.add(new Status("Tất cả",Color.rgb(0,0,0)));
                arrayListStatus.add(new Status("Chưa thực hiện",Color.rgb(116,115,115)));
                arrayListStatus.add(new Status("Đang thực hiện trong hạn",Color.rgb(58,119,252)));
                arrayListStatus.add(new Status("Đang thực hiện quá hạn",Color.rgb(241,67,67)));
                arrayListStatus.add(new Status("Hoàn thành đúng hạn",Color.rgb(76,166,65)));
                arrayListStatus.add(new Status("Hoàn thành quá hạn",Color.rgb(236,156,82)));
                arrayListStatus.add(new Status("Chờ duyệt hoàn thành",Color.rgb(204,17,192)));
                arrayListStatus.add(new Status("Tạm dừng",Color.rgb(0,0,0)));
                arrayListStatus.add(new Status("Đã hủy",Color.rgb(132,7,7)));
                adapterColor = new CustomAdapter(arrayListStatus);
                spinner.setAdapter(adapterColor);
                break;
        }
    }
}
