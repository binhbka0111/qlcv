package com.binhdi0111.bka.customview_mvp.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.binhdi0111.bka.customview_mvp.Interface.InterfaceCreateJob;
import com.binhdi0111.bka.customview_mvp.Myservice;
import com.binhdi0111.bka.customview_mvp.Presenter.CreateJobPresenter;
import com.binhdi0111.bka.customview_mvp.R;


public class CreateJob extends AppCompatActivity implements InterfaceCreateJob {
    EditText edt1,edtDate1,edtDate2,edtTime1,edtTime2;
    ImageView imgDate1,imgDate2,imgTime1,imgTime2,imgBack;
    Button btnFile,btncomfirm;
    CreateJobPresenter createJobPresenter;
    Spinner spinner1,spinner2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_job);
        createJobPresenter = new CreateJobPresenter(this);
        initView();
        createJobPresenter.SetAdapterSpiner(1,spinner1);
        createJobPresenter.SetAdapterSpiner(2,spinner2);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CreateJob.this,MainActivity.class);
                startActivity(intent);
            }
        });

        btncomfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CreateJob.this, Myservice.class);
                startService(intent);
            }
        });


        edt1.setOnKeyListener(onSoftKeyboardDonePress);

        imgDate1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createJobPresenter.DatePicker(edtDate1);
            }
        });

        imgDate2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createJobPresenter.DatePicker(edtDate2);
            }
        });

        imgTime1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createJobPresenter.TimePicker(edtTime1);
            }
        });

        imgTime2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createJobPresenter.TimePicker(edtTime2);
            }
        });

        btnFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(CreateJob.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},createJobPresenter.RequestCode());
            }
        });
    }

    public void initView(){
        imgBack =(ImageView)findViewById(R.id.imageView2);
        imgTime2 =(ImageView) findViewById(R.id.imageViewTime2);
        edtTime2 = (EditText) findViewById(R.id.editTextTime4);
        imgTime1 =(ImageView) findViewById(R.id.imageViewTime1);
        edtTime1 = (EditText) findViewById(R.id.editTextTime3);
        imgDate2 = (ImageView) findViewById(R.id.imageViewDate1);
        edtDate2 = (EditText) findViewById(R.id.editTextTime2);
        btnFile = (Button) findViewById(R.id.buttonFile);
        imgDate1 = (ImageView) findViewById(R.id.imageViewDate);
        edtDate1 = (EditText) findViewById(R.id.editTextTime1);
        edt1 = (EditText) findViewById(R.id.editText1);
        btncomfirm = (Button)findViewById(R.id.button3);
        this.spinner2= (Spinner)findViewById(R.id.Spinner2);
        this.spinner1 = (Spinner) findViewById(R.id.spinner1);
    }
    private View.OnKeyListener onSoftKeyboardDonePress=new View.OnKeyListener()
    {
        public boolean onKey(View v, int keyCode, @NonNull KeyEvent event)
        {
            if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)
            {
                // code to hide the soft keyboard
                edt1.clearFocus();
                edt1.requestFocus(EditText.FOCUS_DOWN);
            }
            return false;
        }
    };
    private ActivityResultLauncher activityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();
                        Uri uri = data.getData();
                    }
                }
            });

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == createJobPresenter.RequestCode() && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            Intent data = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            data.setType("*/*");
            data = Intent.createChooser(data,"choose a file");
            activityResultLauncher.launch(data);
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
