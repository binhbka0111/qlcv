package com.binhdi0111.bka.customview_mvp.BottomSheetDialogFragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.binhdi0111.bka.customview_mvp.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class BottomSheetConfirmJob extends BottomSheetDialogFragment {
    static  BottomSheetConfirmJob bottomSheetConfirmJob;
    static DialogUpdate listener;
    LinearLayout lnCancel,lnUpdate;
    String content,title,contentPriorityLevel;
    int colorPriorityLevel;
    TextView txtTitle;
    public BottomSheetConfirmJob(DialogUpdate listener){
        this.listener = listener;
    }

    public static BottomSheetConfirmJob newInstance(DialogUpdate listener) {
        bottomSheetConfirmJob = new BottomSheetConfirmJob(listener);
        return bottomSheetConfirmJob;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(bottomSheetConfirmJob.STYLE_NORMAL, R.style.DialogStyle);
    }
    @SuppressLint("MissingInflatedId")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottom_sheet_dialog_fragment_confirm,container,false);
        setStyle(bottomSheetConfirmJob.STYLE_NORMAL, R.style.DialogStyle);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
           content =  bundle.getString("content");
           title  = bundle.getString("title");
           contentPriorityLevel = bundle.getString("contentPriorityLevel");
           colorPriorityLevel = bundle.getInt("colorPriorityLevel");
        }
        txtTitle = (TextView)view.findViewById(R.id.textViewTitlejob);
        txtTitle.setText(title);
        lnCancel = (LinearLayout) view.findViewById(R.id.linearLayoutCancelDialogConfrim);
        lnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetConfirmJob.dismiss();
            }
        });
        lnUpdate = (LinearLayout) view.findViewById(R.id.linearLayoutUpdate);
        lnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.clicklistenerUpdateJobGroup(content);
                listener.clickListenerPriorityLevel(contentPriorityLevel,colorPriorityLevel);
                Log.d("mmmmmmmmmmmmmmm", "onClick: "+content);
                bottomSheetConfirmJob.dismiss();
            }
        });



        return  view;
    }



    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        View contentView = View.inflate(getContext(), R.layout.bottom_sheet_dialog_fragment_confirm, null);
        dialog.setContentView(contentView);
        ((View) contentView.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
    }

    public interface DialogUpdate {
        public void clicklistenerUpdateJobGroup(String content);
        public void clickListenerPriorityLevel(String content,int color);
    }
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (DialogUpdate) getActivity();
        }
        catch (ClassCastException e) {
            Log.e("000000000000", "onAttach: ClassCastException: "
                    + e.getMessage());
        }
    }
}

