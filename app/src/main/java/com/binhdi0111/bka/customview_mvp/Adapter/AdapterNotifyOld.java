package com.binhdi0111.bka.customview_mvp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.binhdi0111.bka.customview_mvp.Object.Notify;
import com.binhdi0111.bka.customview_mvp.R;

import java.util.ArrayList;

public class AdapterNotifyOld extends RecyclerView.Adapter<AdapterNotifyOld.ViewHolder>{
    Context context;
    ArrayList<Notify> arrayList;

    public AdapterNotifyOld(Context context, ArrayList<Notify> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_notify_old,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Notify notify = arrayList.get(position);
        holder.txtTitle.setText(notify.getTitle());
        holder.txtMessage.setText(notify.getMessage());
        holder.txtTime.setText(notify.getTime());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtTitle,txtMessage,txtTime;
        ImageView img1,img2;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.titlethienthi);
            txtMessage =(TextView) itemView.findViewById(R.id.contenthienthi);
            txtTime = (TextView) itemView.findViewById(R.id.thoigianhienthi);

        }
    }
}
