package com.binhdi0111.bka.customview_mvp.Fragment;


import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.binhdi0111.bka.customview_mvp.Interface.InterfaceMyJob;
import com.binhdi0111.bka.customview_mvp.Presenter.MyJobPresenter;
import com.binhdi0111.bka.customview_mvp.R;
import com.github.mikephil.charting.charts.PieChart;


public class Fragment_Job extends Fragment implements InterfaceMyJob {
    View view1,view2;
    TextView txtDuocGiao,txtDaGiao;
    ConstraintLayout layout1,layout2;
    MyJobPresenter myJobPresenter;
    RecyclerView recyclerView;
    PieChart pieChart;
    Spinner spinner1,spinner2,spinner3,spinner4,spinner5;
    LinearLayout linearLayoutMyJob;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_job,container,false);
        initView(view);
        linearLayoutMyJob.setNestedScrollingEnabled(false);
        myJobPresenter.SetAdapterSpinner(1,spinner1);
        myJobPresenter.SetAdapterSpinner(2,spinner2);
        myJobPresenter.SetAdapterSpinner(3,spinner3);
        myJobPresenter.SetAdapterSpinner(4,spinner4);
        myJobPresenter.SetAdapterSpinner(5,spinner5);
        myJobPresenter.PieChart(pieChart);
        myJobPresenter.SetAdapterRecyclerView(recyclerView,getActivity());
        layout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myJobPresenter.onClickChangeLayout(0);
            }
        });
        layout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myJobPresenter.onClickChangeLayout(1);
            }
        });
        return view;
    }

    public void initView(View view){
        myJobPresenter = new MyJobPresenter(this);
        spinner1 = (Spinner) view.findViewById(R.id.spinnerJob1);
        spinner2 = (Spinner) view.findViewById(R.id.spinnerJob2);
        spinner3 = (Spinner) view.findViewById(R.id.spinnerJob3);
        spinner4 = (Spinner) view.findViewById(R.id.spinnerJob4);
        spinner5 = (Spinner) view.findViewById(R.id.spinnerJob5);
        pieChart = view.findViewById(R.id.piechartJob);
        recyclerView  =(RecyclerView) view.findViewById(R.id.recyclerViewJob);
        linearLayoutMyJob  = (LinearLayout) view.findViewById(R.id.linearLayoutMyJob);
        txtDuocGiao = (TextView) view.findViewById(R.id.textViewMyJobDuocgiao);
        view1 = (View)view.findViewById(R.id.viewMyJob1);
        txtDaGiao = (TextView) view.findViewById(R.id.textViewMyJobDagiao);
        view2 = (View) view.findViewById(R.id.viewMyJob2);
        layout1 = (ConstraintLayout)view.findViewById(R.id.layoutMyJob1);
        layout2 = (ConstraintLayout)view.findViewById(R.id.layoutMyJob2);
    }

    @Override
    public void SelectTab0() {
        view1.setVisibility(View.VISIBLE);
        view2.setVisibility(View.INVISIBLE);
        txtDuocGiao.setTypeface(null, Typeface.BOLD);
        txtDaGiao.setTypeface(null,Typeface.NORMAL);
        layout1.setBackground(getResources().getDrawable(R.drawable.custom1));
        layout2.setBackground(getResources().getDrawable(R.drawable.custom2));
    }

    @Override
    public void SelectTab1() {
        view2.setVisibility(View.VISIBLE);
        view1.setVisibility(View.INVISIBLE);
        txtDaGiao.setTypeface(null, Typeface.BOLD);
        txtDuocGiao.setTypeface(null,Typeface.NORMAL);
        layout1.setBackground(getResources().getDrawable(R.drawable.custom4));
        layout2.setBackground(getResources().getDrawable(R.drawable.custom3));
    }
}
