package com.binhdi0111.bka.customview_mvp.Object;

public class Item {
    private  int index;
    private String name;
    private int image;

    public Item(int index, String name, int image) {
        this.index = index;
        this.name = name;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
