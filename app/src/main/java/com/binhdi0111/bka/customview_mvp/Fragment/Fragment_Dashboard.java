package com.binhdi0111.bka.customview_mvp.Fragment;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.binhdi0111.bka.customview_mvp.Activity.MainActivity;
import com.binhdi0111.bka.customview_mvp.Activity.Personnel;
import com.binhdi0111.bka.customview_mvp.Activity.CreateJob;
import com.binhdi0111.bka.customview_mvp.Activity.statistical;
import com.binhdi0111.bka.customview_mvp.Interface.InterfaceDashboard;
import com.binhdi0111.bka.customview_mvp.Presenter.DashboardPresenter;
import com.binhdi0111.bka.customview_mvp.R;
import com.github.mikephil.charting.charts.PieChart;


public class Fragment_Dashboard extends Fragment implements InterfaceDashboard {
    TextView txtTime,txtDuocGiao,txtDaGiao;
    public static PieChart pieChart;
    RecyclerView recyclerView;
    View view1,view2;
    ConstraintLayout layout1,layout2,constraintLayoutMyJob,constraintLayoutStatistical,constraintLayoutPersonnel,constraintLayoutCreateJob;
    ImageView imgTaoCongViec,imgNotify;
    DashboardPresenter dashboaedPresenter;
    Spinner spinner5,spinner1,spinner2,spinner3,spinner4;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard,container,false);
        initView(view);

        layout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dashboaedPresenter.onClickChangeLayout(0);
            }
        });
        layout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dashboaedPresenter.onClickChangeLayout(1);
            }
        });
        constraintLayoutPersonnel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dashboaedPresenter.TransferActivity(2);
            }
        });
        constraintLayoutStatistical.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dashboaedPresenter.TransferActivity(1);
            }
        });
        constraintLayoutCreateJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dashboaedPresenter.TransferActivity(3);
            }
        });
        constraintLayoutMyJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dashboaedPresenter.TransferActivity(4);
            }
        });
        imgNotify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.putExtra("imgNotify", "1");
                startActivity(intent);
            }
        });
        dashboaedPresenter.SetAdapterSpinner(1,spinner1);
        dashboaedPresenter.SetAdapterSpinner(2,spinner2);
        dashboaedPresenter.SetAdapterSpinner(3,spinner3);
        dashboaedPresenter.SetAdapterSpinner(1,spinner4);
        dashboaedPresenter.SetAdapterSpinner(2,spinner5);
        dashboaedPresenter.SetApdaterRecyclerView(recyclerView, getActivity());
        dashboaedPresenter.PieChart(pieChart);
        dashboaedPresenter.DatePicker(txtTime);

        return view;
    }

    @Override
    public void goStatistical() {
        Intent intent = new Intent(getActivity(), statistical.class);
        startActivity(intent);
    }
    @Override
    public void goPersonnel() {
        Intent intent = new Intent(getActivity(), Personnel.class);
        startActivity(intent);
    }

    @Override
    public void goCreateJob() {
        Intent intent = new Intent(getActivity(), CreateJob.class);
        startActivity(intent);
    }

    @Override
    public void goMyJob() {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.putExtra("layoutMyJob", "1");
        startActivity(intent);
    }

    public  void initView(View view){
        dashboaedPresenter = new DashboardPresenter(this);
        txtTime = (TextView)view.findViewById(R.id.textViewTime);
        imgTaoCongViec =(ImageView)view.findViewById(R.id.imageView8);
        txtDuocGiao = (TextView) view.findViewById(R.id.textView18);
        view1 = (View)view.findViewById(R.id.view1);
        txtDaGiao = (TextView) view.findViewById(R.id.textViewMyJobDagiao);
        view2 = (View) view.findViewById(R.id.view2);
        layout1 = (ConstraintLayout)view.findViewById(R.id.layouDashboard1);
        layout2 = (ConstraintLayout)view.findViewById(R.id.layouDashboard2);
        pieChart = (PieChart) view.findViewById(R.id.piechart);
        constraintLayoutStatistical = (ConstraintLayout)view.findViewById(R.id.constraintLayoutThongke);
        constraintLayoutPersonnel  = (ConstraintLayout)view.findViewById(R.id.constraintLayoutpersonnel);
        constraintLayoutCreateJob = (ConstraintLayout)view.findViewById(R.id.constraintLayout4);
        constraintLayoutMyJob = view.findViewById(R.id.constraintLayoutMyJob);
        imgNotify = view.findViewById(R.id.imageViewNotify);
        recyclerView = view.findViewById(R.id.recyclerView);
        this.spinner1 = (Spinner) view.findViewById(R.id.spinnerDashboard1);
        this.spinner2 = (Spinner) view.findViewById(R.id.spinnerDashboard2);
        this.spinner3 = (Spinner) view.findViewById(R.id.spinnerDashboard3);
        this.spinner4 = (Spinner) view.findViewById(R.id.spinnerDashboard4);
        this.spinner5 = (Spinner) view.findViewById(R.id.spinnerDashboard5);
    }


    @Override
    public void SelectTab0() {
        view1.setVisibility(View.VISIBLE);
        view2.setVisibility(View.INVISIBLE);
        txtDuocGiao.setTypeface(null, Typeface.BOLD);
        txtDaGiao.setTypeface(null,Typeface.NORMAL);
        layout1.setBackground(getResources().getDrawable(R.drawable.custom1));
        layout2.setBackground(getResources().getDrawable(R.drawable.custom2));
    }

    @Override
    public void SelectTab1() {
        view2.setVisibility(View.VISIBLE);
        view1.setVisibility(View.INVISIBLE);
        txtDaGiao.setTypeface(null, Typeface.BOLD);
        txtDuocGiao.setTypeface(null,Typeface.NORMAL);
        layout1.setBackground(getResources().getDrawable(R.drawable.custom4));
        layout2.setBackground(getResources().getDrawable(R.drawable.custom3));
    }


}
