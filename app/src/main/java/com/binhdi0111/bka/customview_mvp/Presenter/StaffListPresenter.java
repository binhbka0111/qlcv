package com.binhdi0111.bka.customview_mvp.Presenter;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import com.binhdi0111.bka.customview_mvp.Adapter.AdapterRoomStaff;
import com.binhdi0111.bka.customview_mvp.Interface.InterfaceStaffList;
import com.binhdi0111.bka.customview_mvp.Object.Room;

import java.util.ArrayList;

public class StaffListPresenter {
    InterfaceStaffList interfaceStaffList;

    AdapterRoomStaff adapterRoomStaff;
    ArrayList<Room> arrayListRoom;

    public StaffListPresenter(InterfaceStaffList interfaceStaffList) {
        this.interfaceStaffList = interfaceStaffList;
    }

    public void setRecyclerStaffList(RecyclerView recyclerView, Context context){
        arrayListRoom = new ArrayList<>();
        arrayListRoom.add(new Room("Lãnh đạo UBND Việt Yên"));
        adapterRoomStaff = new AdapterRoomStaff(context,arrayListRoom);
        recyclerView.setAdapter(adapterRoomStaff);
        recyclerView.setNestedScrollingEnabled(false);
    }

}
