package com.binhdi0111.bka.customview_mvp.Presenter;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.binhdi0111.bka.customview_mvp.Adapter.CustomAdaperSpinner;
import com.binhdi0111.bka.customview_mvp.Interface.InterfaceUpdateJobGroup;
import com.binhdi0111.bka.customview_mvp.Object.spinner;

import java.util.ArrayList;

public class UpdateJobGroupPresenter {
    InterfaceUpdateJobGroup interfaceUpdateJobGroup;
    CustomAdaperSpinner adapter;
    ArrayList<spinner> arrayList = new ArrayList<>();
    String result;
    public UpdateJobGroupPresenter(InterfaceUpdateJobGroup interfaceUpdateJobGroup) {
        this.interfaceUpdateJobGroup = interfaceUpdateJobGroup;
    }

    public String SetAdapterSpinner(Spinner spinner){
        arrayList.add(new spinner("Chưa chọn nhóm công việc"));
        arrayList.add(new spinner("GIẢI PHÓNG MẶT BẰNG XÃ HƯƠNG MAI"));
        arrayList.add(new spinner("DỰ ÁN QUẢN LÝ CÔNG VIỆC HUYỆN VIỆT YÊN"));
        arrayList.add(new spinner("CHUYỂN ĐỔI SỐ THỊ TRẤN BÍCH ĐỘNG"));
        arrayList.add(new spinner("Dự án Xã - Thị trấn"));
        arrayList.add(new spinner("Dự án Các đơn vị của Huyện"));
        arrayList.add(new spinner("Dự án xây dựng nhà tình nghĩa"));
        adapter = new CustomAdaperSpinner(arrayList);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                result = arrayList.get(i).getName();
                Log.d("gggggggggggggggg", "onCreate: "+result);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        return result;
    }


}
