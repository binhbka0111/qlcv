package com.binhdi0111.bka.customview_mvp.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.binhdi0111.bka.customview_mvp.Activity.ChangePassword;
import com.binhdi0111.bka.customview_mvp.Activity.UserInfomation;
import com.binhdi0111.bka.customview_mvp.BottomSheetDialogFragment.BottomSheetDialogCamera;
import com.binhdi0111.bka.customview_mvp.BottomSheetDialogFragment.BottomSheetExit;
import com.binhdi0111.bka.customview_mvp.BroadCastReceiver;
import com.binhdi0111.bka.customview_mvp.Interface.InterfaceUser;
import com.binhdi0111.bka.customview_mvp.Presenter.UserPresenter;
import com.binhdi0111.bka.customview_mvp.R;

public class Fragment_User extends Fragment implements InterfaceUser {
    UserPresenter userPresenter;
    RecyclerView recyclerView;
    ImageView imgTouchID, imgChangeInformationUser, imgCamera, imgAvatar;
    LinearLayout  lnChangePassword, lnExit;
    BottomSheetDialogCamera bottomSheetDialogCamera;
    public boolean touchID = false;
    BroadCastReceiver broadCastReceiver = new BroadCastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent) {
            super.onReceive(context, intent);
            if (intent != null){
                String path = intent.getStringExtra("file");
                Log.d("mmmmmmmmmmmmmmmmmmm", "onReceive: "+path);
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(path,bmOptions);
                Log.d("hehehehehehhehe", "onReceive: "+bitmap);
                imgAvatar.setImageBitmap(bitmap);
                bottomSheetDialogCamera.dismiss();
            }
        }
    };
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user,container,false);
        initView(view);
        userPresenter = new UserPresenter(this);
        lnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ChangePassword.class);
                startActivity(intent);
            }
        });

        imgTouchID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (touchID==false){
                    imgTouchID.setImageResource(R.drawable.b12);
                    touchID = true;
                }else {
                    dialog();
                }
            }
        });
        imgChangeInformationUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), UserInfomation.class);
                startActivity(intent);
            }
        });

        imgCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickOpenBottomDialog();
            }
        });

        lnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openBottomExit();
            }
        });
        userPresenter.SetRecyclerView(recyclerView,getActivity());
        IntentFilter filter = new IntentFilter("send_file_data");
        getActivity().registerReceiver(broadCastReceiver,filter);
        return view;
    }
    public void initView(View view){
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerViewUser);
        lnChangePassword = (LinearLayout) view.findViewById(R.id.linearLayoutChangePassword);
        imgTouchID = (ImageView) view.findViewById(R.id.imageTouchID);
        imgChangeInformationUser = (ImageView) view.findViewById(R.id.imageChangeInformationUser);
        imgCamera = (ImageView) view.findViewById(R.id.imageViewCamera);
        lnExit = (LinearLayout) view.findViewById(R.id.exit);
        imgAvatar = (ImageView) view.findViewById(R.id.circleImageView);
    }
    private void openBottomExit() {
        BottomSheetExit bottomSheetDialog = BottomSheetExit.newInstance();
        bottomSheetDialog.show(getActivity().getSupportFragmentManager(), "Bottom Sheet Dialog Fragment");
    }

    private void clickOpenBottomDialog() {
        bottomSheetDialogCamera = new BottomSheetDialogCamera();
        bottomSheetDialogCamera.show(getActivity().getSupportFragmentManager(), bottomSheetDialogCamera.getTag());
    }

    private void dialog(){
        Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_touchid);
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        LinearLayout linearLayoutBack = (LinearLayout) dialog.findViewById(R.id.lnTouchIDBack);
        linearLayoutBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        LinearLayout linearLayoutConfirm = (LinearLayout) dialog.findViewById(R.id.lnTouchIDConfirm);
        linearLayoutConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                imgTouchID.setImageResource(R.drawable.b9);
                touchID =false;
            }
        });


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(broadCastReceiver);
    }
}
