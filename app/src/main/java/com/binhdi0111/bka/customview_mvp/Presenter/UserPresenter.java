package com.binhdi0111.bka.customview_mvp.Presenter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import com.binhdi0111.bka.customview_mvp.Adapter.AdapterUser;
import com.binhdi0111.bka.customview_mvp.Interface.InterfaceUser;
import com.binhdi0111.bka.customview_mvp.Object.Item;
import com.binhdi0111.bka.customview_mvp.R;
import com.binhdi0111.bka.customview_mvp.Activity.StaffList;

import java.util.ArrayList;

public class UserPresenter  {
    InterfaceUser interfaceUser;
    ArrayList<Item> arrayList;
    AdapterUser adapter;
    public UserPresenter(InterfaceUser interfaceUser) {
        this.interfaceUser = interfaceUser;
    }
    public void SetRecyclerView(RecyclerView recyclerView, Context context){
        arrayList = new ArrayList<>();
        arrayList.add(new Item(1,"LĐ UBND Việt Yên", R.drawable.a7));
        arrayList.add(new Item(2,"Chủ tịch", R.drawable.b1));
        arrayList.add(new Item(3,"Cán bộ", R.drawable.b1));
        arrayList.add(new Item(4,"0123456789", R.drawable.b2));
        arrayList.add(new Item(5,"binhbka0111@gmail.com", R.drawable.b3));
        arrayList.add(new Item(6,"01/11/2000", R.drawable.b4));
        arrayList.add(new Item(7,"Thái Bình", R.drawable.b5));
        arrayList.add(new Item(8,"0123456789", R.drawable.b6));
        arrayList.add(new Item(9,"0123456789", R.drawable.b7));
        adapter = new AdapterUser(context, new AdapterUser.ItemClick() {
            @Override
            public void onClickItem(int index) {

                switch (arrayList.get(index).getIndex()){
                    case 1:
                        Intent intent = new Intent(context, StaffList.class);
                        context.startActivity(intent);
                        break;
                }
            }
        }, arrayList);
        recyclerView.setAdapter(adapter);
    }

}
