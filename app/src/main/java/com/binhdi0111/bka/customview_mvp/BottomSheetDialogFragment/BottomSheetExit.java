package com.binhdi0111.bka.customview_mvp.BottomSheetDialogFragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.binhdi0111.bka.customview_mvp.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class BottomSheetExit extends BottomSheetDialogFragment {
    LinearLayout lnCanel;
    static BottomSheetExit bottomSheetExit;

    public static BottomSheetExit newInstance() {
        bottomSheetExit = new BottomSheetExit();
        return bottomSheetExit;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottom_sheet_exit,container,false);
        lnCanel = (LinearLayout) view.findViewById(R.id.linearLayoutCancel);
        lnCanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetExit.dismiss();
            }
        });
        return view;
    }
    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        View contentView = View.inflate(getContext(), R.layout.bottom_sheet_exit, null);
        dialog.setContentView(contentView);
        ((View) contentView.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
    }
}
