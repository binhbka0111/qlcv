package com.binhdi0111.bka.customview_mvp.BottomSheetDialogFragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.binhdi0111.bka.customview_mvp.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class BottomSheetDialogDocument extends BottomSheetDialogFragment {
    static DialogDocument listener;
    @SuppressLint("StaticFieldLeak")
    static BottomSheetDialogDocument bottomSheetDialogDocument;
    EditText edtName,edtSymbolCode,edtDate,edtNote;
    LinearLayout lnCancel,lnConfirm;
    Calendar calendar;
    SimpleDateFormat simpleDateFormat;
    ImageView imgDate;
    String resultSymbolCode,resultNameDocument, resultDateDocument, resultNoteDocument;
    public BottomSheetDialogDocument(DialogDocument listener) {
        BottomSheetDialogDocument.listener = listener;
    }

    public static BottomSheetDialogDocument newInstance(DialogDocument listener) {
        bottomSheetDialogDocument= new BottomSheetDialogDocument(listener);
        return bottomSheetDialogDocument;
    }

    @SuppressLint("MissingInflatedId")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottom_sheet_dialog_fragment_document,container,false);
        edtSymbolCode =view.findViewById(R.id.editTextSymbolCode);
        edtName = view.findViewById(R.id.editTextNameDocument);
        edtDate =  view.findViewById(R.id.editTextDateDocument);
        edtNote =  view.findViewById(R.id.editTextNoteDocument);
        imgDate =view.findViewById(R.id.imageViewDocumnetDate);
        imgDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePicker(edtDate);
            }
        });

        lnCancel = (LinearLayout) view.findViewById(R.id.linearLayoutCancelDocument);
        lnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogDocument.dismiss();
            }
        });
        lnConfirm =(LinearLayout) view.findViewById(R.id.linearLayoutConfirmDocument);
        lnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String symbolcode = edtSymbolCode.getText().toString().trim();
                String name = edtName.getText().toString().trim();
                String date = edtDate.getText().toString().trim();
                String note = edtNote.getText().toString().trim();
                listener.clickListener(symbolcode,name,date,note);
                bottomSheetDialogDocument.dismiss();
            }
        });
        Bundle bundle = this.getArguments();
        if(bundle!=null){
            resultSymbolCode = bundle.getString("symbolcode");
            resultNameDocument = bundle.getString("name");
            resultDateDocument = bundle.getString("date");
            resultNoteDocument = bundle.getString("note");
            edtSymbolCode.setText(resultSymbolCode);
            edtName.setText(resultNameDocument);
            edtDate.setText(resultDateDocument);
            edtNote.setText(resultNoteDocument);
        }
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DialogStyle);
    }
    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        View contentView = View.inflate(getContext(), R.layout.bottom_sheet_dialog_detail_job, null);
        dialog.setContentView(contentView);
        ((View) contentView.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
    }
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (DialogDocument) getActivity();
        }
        catch (ClassCastException e) {
            Log.e("000000000000", "onAttach: ClassCastException: "
                    + e.getMessage());
        }
    }
    public interface DialogDocument{
        void clickListener(String symbolCode,String Name,String Date,String Note);
    }
    private void DatePicker(TextView textView){
        calendar = Calendar.getInstance();
        int ngay = calendar.get(Calendar.DATE);
        int thang = calendar.get(Calendar.MONTH);
        int nam = calendar.get(Calendar.YEAR);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("SimpleDateFormat")
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                calendar.set(i,i1,i2);
                simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                textView.setText(simpleDateFormat.format(calendar.getTime()));
            }
        },nam,thang,ngay);
        datePickerDialog.show();
    }

}
