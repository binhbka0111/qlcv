package com.binhdi0111.bka.customview_mvp.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.binhdi0111.bka.customview_mvp.R;
import com.binhdi0111.bka.customview_mvp.Object.Staff;

import java.util.ArrayList;

public class AdapterStaffChild extends RecyclerView.Adapter<AdapterStaffChild.ViewHolderStaffChild>{
    Context context;
    ArrayList<Staff> arrayList;
    ItemClick itemClick;

    public AdapterStaffChild(Context context, ArrayList<Staff> arrayList, ItemClick itemClick) {
        this.context = context;
        this.arrayList = arrayList;
        this.itemClick = itemClick;
    }

    @NonNull
    @Override
    public ViewHolderStaffChild onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_staff_child,parent,false);
        return new ViewHolderStaffChild(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderStaffChild holder, int position) {
        Staff staff =arrayList.get(position);
        holder.txtStaffRole.setText(staff.getRole());
        holder.txtStaffName.setText(staff.getName());
        holder.imgAvatar.setImageResource(staff.getAvatar());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClick.onClickItem(holder.getAdapterPosition());
            }
        });
        holder.btnDetailStaff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("ooooooooo", String.valueOf(holder.getAdapterPosition()));
            }
        });

        if(position == arrayList.size()-1){
            holder.view.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolderStaffChild extends RecyclerView.ViewHolder {
        TextView txtStaffName,txtStaffRole;
        ImageView imgAvatar;

        Button btnDetailStaff;

        View view;
        public ViewHolderStaffChild(@NonNull View itemView) {
            super(itemView);
            txtStaffName = (TextView)  itemView.findViewById(R.id.txtStaffName);
            txtStaffRole = (TextView) itemView.findViewById(R.id.textViewStaffRole);
            imgAvatar = (ImageView) itemView.findViewById(R.id.imageViewAvatar);
            btnDetailStaff = (Button) itemView.findViewById(R.id.detailStaff);
            view = (View) itemView.findViewById(R.id.underLineStaff);
        }
    }
    public interface ItemClick{
        void onClickItem(int index);
    }
}
